# Week 7: Network Layer: Data Plane

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 7 Vegetables](Veggies.md)
* [Week 7 Learning Goals](Wk7_LearningGoals.md)
* [Week 7 Deep Dive Lab](Networks_Week_7_Lab.pdf)
* [Week 7 Peer Review](https://gitlab.usna.edu/ndzayfman/ic322/-/issues/21)
