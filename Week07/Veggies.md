# Week 7 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 4.2

### R11. Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).
Whenever queuing is involved, packet loss can occur. It is at the queues where packets are lost and dropped. HOL (head of the line) blocking makes it so that a packet in a specific queue must wait if another packet from a different queue is already going to the same destination. This prevents packets from behind the one waiting from being sent off, even if the ones behind the front packet are not going to the same location.
Example: dark blue packets at bottom and top are going to same location. Top dark blue packet goes first, meaning bottom blue must wait. Because of this, the light blue packet behind the dark blue on the bottom must wait till the top dark blue packet is delivered and then the bottom dark blue packet is delivered. As you can see, the middle gray packet is not waiting behind another packet to get to its destination, so it can simply go as needed.
Increasing the speed of the transmission of packets can help cause packet loss be eliminated.
![R1 Graphic](<R1pic.png>)

### R12. Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?
The number of queued packets can grow large enough to exhaust available memory at the output port. When this happens at the output port, the last packet is dropped (drop-tail) or it removes one or more already-queued packets to make room for the newly arrived packet. Sometimes, the packet is marked before it is dropped so a congestion signal is sent to the sender. If the switch fabric speed is increased to where the transmission speed is N * N what it is, then packet loss will be switch fabric speed. (This will actually NOT help)

### R13. What is HOL blocking? Does it occur in input ports or output ports?
Head-of-the-line blocking is an input-queued switch. This is explained in R11.

### R16. What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?
RR = round robin
  * packets are sorted into classes following the priority queuing method
  * instead of finishing all the packets in the top priority, the round robin will circle through the different classes
  * ex: trasnmit class 1 packet, then trasnmit class 2 packet, then trasnmit class 3 packet ...
WFQ = weighted fair queueing
  * each class may receive a differential amount of service in any integral of time
  * each class assigned a weight
  * during any interval of time where a specific class is to have packets sent, it is guarenteeds a fraction of service equal to its assigned weight divided by the sum of the weights of all classes

## Section 4.3

### R18. What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?
The time-to-live (TTL) feild is included to ensure that datagrams do not circulate forever in the network. This field is decremented by one each time the datagram is processed by a router. If the TTL field reaches 0, a router must drop that datagram.

### R21. Do routers have IP addresses? If so, how many?
An IP address is technically associated with an interface rather with the host or router containing that interface.

## Problems

### P4. Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?
![P4 Graphic](<switchfabric.png>)

The smallest number of time slots needed are 3 since there are only 3 input ports. You would go, X on top & Y in middle, then X in middle & Y on bottom, then Z on bottom.
THe largest is 5 because instead the packets can go one by one.

### P5. Suppose that the WEQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.

#### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .).
* Assume: class A = .5, class B = .25 & class C = .25
* They are guaranteed their weight / sum of all weights, so:
  * total = 1
  * A = .5/1 = 1/2
  * B = .25/1 = 1/4
  * C = .25/1 = 1/4
* A A B C

#### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?
* WQF is work conserving so it will immediately move on to the next class in the service sequence when it finds an empty class queue
* A A B C

### P8. Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:
![P8 Graphic](<P8pic.PNG>)

#### a. Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.

|Destination Address Range|Link Interface|
|------|------|
|11100000 00|0|
|11100000 01000000|1|
|1110000|2|
|11100001 1|3|
|otherwise|3|

#### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:
![P8b Graphic](<P8bpic.PNG>)

* 1st address: goes through and matches none, so it goes to the otherwise (link 3)
* 2nd address: matches with link 2
* 3rd address: goes through and matches link 3 address, which is the "not" link 2 address, so it goes with link 3

### P9. Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table:
![P9 Graphic](<P9pic.PNG>)

### For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.
|Interface|Range of Destination Host|Number of Addresses in Range|
|----|----|----|
|0|00**000000** - 00**111111**|2^6 = 64|
|1|010**00000** - 010**11111**|2^5 = 32|
|2|011**00000** - 011**11111**|2^5 = 32|
|2|10**000000** - 10**111111**|2^6 = 64|
|3|11**000000** - 11**111111**|2^6 = 64|

### P11. Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.

* subnet 1: 60 interfaces
  * 2^6 = 64
* subnet 2: 90 interfaces
  * 2^7 = 128
* subnet 3: 12 interfaces
  * 2^4 = 16

*   223   .   1    .   17   .   -    /24
* 11011111 00000001 00010001 --------
  * 24 bits matter, the last 8 don't

* subnet 1 needs: 6 bits for its interfaces --000000
  * 1: needs to read 26 bits
  * subnet 1 = 223.1.17/26
* subnet 2 needs: 7 bits for its interfaces -0000000
  * 2: needs to read 25 bits
  * subnet 2 = 223.1.17/25
* subnet 3 needs: 4 bits for its interfaces ----0000
  * 3: needs to read 28 bits
  * subnet 3 = 223.1.17/28



