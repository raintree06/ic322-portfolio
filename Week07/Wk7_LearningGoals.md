# Week 7 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.
* Also called an *IP Network* or a *network*
* A subnet is in broad terms, a network inside of a network. It makes network traffic more efficient because data can travel in shorter distances and avoid going through extra routers and such to deliver information.
* Subnet mask is the portion of an IP address that contains the information pertaining to the subnet rather than the host
  * For example, if you have IP address 223.1.1.0/24, then the **/24** part would be the subnet mask, because it indicates that the leftmost 24 bits of the 32-bit quantity will pertain to the subnet.
  * Another example:
    Tier-1 IP address would be 30.0.0.0/8
    Regional IP address connected to Tier-1 above would be 30.1.0.0*/16*
    * The */16* in the regional IP address would be the subnet mask indicating that the leftmost 16 bits pertain to the subnet
* When packets are forwarded to links, the first way it sorts which link interface it is to be send is by matching the prefix of the packet's destination address with the link's prefix address. However, if there are multiple matches outside of the first 3 links, then the router must use **longest prefix matching rule** in which instead of comparing the prefix of bits, it will compare the longest matching entry of all the bits
  * This works with subnets because the routers want to match the address with whatever is most close to where the data is supposed to be sent
  * Example: you have two networks 200.23.21.0/23 and 200.23.22.0/23, you are transmitting data to address 200.23.21.62
      At first, the router may check the first 2 parts of the address, **200.23** (aka *prefix*), but because both addresses match the destination address, the router must use **longest prefix matching* to find where to send the data.

## I can step though the DHCP protocol and show how it is used to assign IP addresses.
* DHCP = Dynamic Host Configuration Protocol
* Situations used: when an organization has obtained a block address and now can assign individual IP addresses to the host and router interfaces in its organization
* Allows a host to be allocated an IP address automatically
* Allows a host to learn additional information such as subnet mask, address of its first-hop router (aka default gateway), and address of its local DNS server
* DHCP does what administrator would have to do manually, so it helps out when creating an organizational address
* aka **plug-and-play** or **zeroconf**
* it is client-server protocol
* 4 step process for network setting:
  1. DHCP server discovery: find a DHCP server to interact with. done using a **DHCP discover message** in which the DHCP client passes the IP datagram of request to the link layer to broadcast that frame to all nodes attatched to the subnet using IP address 255.255.255.255 or 0.0.0.0
  * 0.0.0.0 is the source, 255.255.255.255 is the destination
  2. DHCP server offer(s): DHCP server responds to the DHCP offer message again broadcasted to all nodes on the subnet with the same address as before. The message offers a transaction ID of received discovered message, the proposed IP address of the client, the network mask, and the IP address lease time (how long the address is valid for the client)
  3. DHCP request: newly arriving client will choose from among 1 or more server offers and respond with a DHCP request message and an echo of the configuration parameters
  4. DHCP ACK: server responds to DHCP request message with a DHCP ACK message to confirm the requested parameters.
