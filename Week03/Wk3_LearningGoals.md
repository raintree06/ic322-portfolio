# Week 3 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate a domain name into an IP address.
Example: suppose DNS client wants to retermine the IP address for the hostname www.amazon.com. Client first contacts one of the root servers, which returns IP addresses for TLD servers for the top-level domain com. The client then contacts one of these TLD servers, which returns the IP address of an authoritative server for amazon.com. Then the client contacts one of the authoritative servers for amazon.com, which returns the IP address for the hostname www.amazon.com.
* Root Servers: provide IP addresses of the TLD servers
* TLD (Top-level Domain) Servers: there is a server for each top-level domain. Provides IP addresses for the authoritative DNS servers
  * examples: com, org, net, edu, gov
* Authoritative DNS Servers: every organization with publicly accessible hosts on the internet must provide publicly accessible DNS records that map the names of those hosts to IP addresses
* local DNS: typically close to the host
  * does not strictly belong to DNS hierarchy above

## I can explain the role of each DNS record type.
Resource record = (Name, Value, Type, TTL)
* Type=A : Name is a hostname and Value is the ip address for the hostname
  * standard hostname-to-IP address mapping
  * ex: (relay1.bar.foo.com, 145,37,93,126, A)
* Type=NS : Name is domain & Value is hostname of authoritative DNS server
  * ex: (foo.com, dns.foo.com, NS)
* Type=CNAME : value is canonical hostname for the alias hostname Name
* Type=MX : value is canonical name of mail server that has alias to hostname Name)

## I can explain the role of the SMTP, IMAP, and POP protocols in the email system.
Transmitting Emails:
* SMTP: transfers messages from senders' mail servers to the recipients' mail servers
  * Uses TCP connection to perform application layer handshaking before transferring info
Retreiving Emails:
* IMAP: retains message on server
* POP: downloads messages directly to device, seperate on different devices

## I know what each of the following tools are used for: nslookup, dig, whois.
* nslookup is used for fidning the IP address corresponding to a known hostname
* whois is used to know who owns a domain, retreived domain registration data & domain owner data
* dig is used for authoritative purposes, DNS query from device to targeted IP address or hostname
