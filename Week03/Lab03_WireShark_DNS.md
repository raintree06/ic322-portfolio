# Lab 3: Wireshark DNS

**Mowery, Sydney** | ***IC322 Networks***

## Intro to the the WireShark DNS Lab

* This lab explored the DNS side of WireShark. It focused on how to read the DNS query and response messages as well as how to read and operate with nslookup. The lab has helped me to better understand the purpose of DNS as a gateway to retreiving information from online.
* Collaberation: none
* [WireShark DNS Lab Instructions](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_DNS_v8.1.doc)

## nslookup

### 1. Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP address of www.iitb.ac.in
The IP address for the web server for the Indian Institute of Tech in Bombay, India is 103.21.124.10.

### 2. What is the IP address of the DNS server that provided the answer to your nslookup command in question 1 above?
The IP address of the DNS server that provided the IP address for question 1 using the nslookup command was 10.1.74.10

### 3. Did the answer to your nslookup command in question 1 above come from an authoritative or non-authoritative server?
The answer to my nslookup in question 1 came from a non-authoritativer server.

### 4. Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain. What is that name? (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?
To find the IP address of an authoritative name server, you would use the option "-type=NS" following the "nslookup" command.
For the site, the name of the authoritative server is dns1.iitb.ac.in.


## The DNS cache on your computer & Tracing DNS w/ Wireshark

### 5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message? Is this query message sent over UDP or TCP?
The packet number in the trace for the DNS query message resolving the name gaia.cs.umass.edu is packet number 3.

The query message saus the protocol is UDP.

### 6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message? Is this response message received via UDP or TCP?
The packet number for the DNS response message is 4. The protocol used is also UDP.

### 7. What is the destination port for the DNS query message? What is the source port of the DNS response message?
The destination port for the DNS query message is 53. The source port of the DNS response message is 53.

### 8. To what IP address is the DNS query message sent?
The DNS query message is sent to IP address 10.1.74.10.

### 9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
The query message contains one query and no answers.

### 10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
The response message contains one query and one answer.

### 11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu. What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/? What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address? What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.
The packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose/ross/ is 49.

The packet number in the trace of the DNS query made to reseolve gaia.cs.umass.edu is 324.

The packet number in the trace of the received DNS response is 326.

The packet number in the trace for the HTTP GET request for the image object .../header_graphic_book_8E2.jpg is 688.

The packet number in the DNS query made to resolve gaia.cs.umass.edu so that this 2nd HTTP request can be sent to the gaia.cs.umass.edu IP address is 762.
DNS caching affects this because this allows the request to bypass some of the extra TCP connections made because the DNS already knows exactly where the query is going and what to pull.

### 12. What is the destination port for the DNS query message? What is the source port of the DNS response message?
The destination port for the DNS query message is 53. The source port of the DNS response message is 53.

### 13. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
The DNS query message is sent to IP address 10.1.74.10. The IP address of the DNS response message is 10.1.74.10.

### 14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?
The DNS query messge uses UDP and does not contain any answers.

### 15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?
The DNS response contains one question ("query") and one answer.

### 16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
The DNS query message is sent to Ip address 10.1.74.10. This is the same IP address of my default loval DNS server.

### 17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?
There are 3 seperate DNS query messages. Each query has one question, no answers.

### 18. Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records
The DNS response message has an "Authoritative nameservers" answer. This includes type SOA, class IN, mname ns1.usna.edu. It also has time to live, data length, and other information.
