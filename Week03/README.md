# Week 3: The Application Layer: DNS

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 3 Vegetables](Veggies.md)
* [Week 3 Learning Goals](Wk3_LearningGoals.md)
* [WireShark DNS Lab](Lab03_WireShark_DNS.md)

