# Week 3 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 2.2 - 2.5

### R16. Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice’s host to Bob’s host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts. 
The internet mail system has 3 major components: user agents, mail servers, and the simple mail transfer protocol (SMTP).

Alice, with a web based e-mail account uses a user agent such as Hotmail or gmail to send her message to her mail server, where the message is placed in the mail server's outgoing message queue. For Bob to read the message, his user agent retreives the message from his mailbox in his mail server.

SMTP is the principal application layer protocol for internet electronic mail.
* uses TCP to transfer mail from sender's mail server to receiver's mail server.
* it is the client side of SMTP on Alice's mail server that opens the TCP connection to the SMTP server on Bob's mail server.

### R18. What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?
The HOL (Head of Line) Blocking problem is encountered when sending all the objects in a web page over a single TCP connection. HTTP/1.1 browsers work around it by opening up multiple parallel TCP connections

HTTP/2 is meant to reduce or get rid of the extra parallel TCP connections. It attempts to change how the data is formatted and transported between the client and server. One way it tries to do this is by  breaking each message into small frames, and interleave the request and response messages on the same TCP connection.


## Section 2.6

### R24. CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them. 
The two server placement philosophies are Enter Deep and Bring Home.

Enter Deep attempts to deploy server clusters in ISPs all over the world. It tries to get close with end users in order to improve user-perceived delay and throughput by decreasing links and routers info must travel. Difficult because maintaining and managing.

Bring Home wants to "bring the ISPs home" by building large clusters at a smaller number of sites. CDNs place their clusters in IXPs. Results in lower maintenance than Enter-Deep design.


## Problems:

### P16. How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.
SMTP makrs the end of a message body with a line consisting of a period ("."). For HTTP, the last line is folowed by an additional carriage return and line feed. HTTP can not use the same method as SMTP because the HTTP message uses a header "content-length" to specifiy how long to message body will be.

### P18 a. What is a whois database? 
A whois database that stores the domain name of registered users.

### P18 b. Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.
I used https://who.is/whois/google.com and https://www.whois.com/whois/.
I found 2 DNS servers are google.com and opendns.com.

### P20. Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain. 
Yes. Find the domains or IP addresses that are most prevalent amonf the cache. These are the most popular.

### P21. Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.
Yes. The dig command allows you to see the recent search for some website used on a DNS server.


