# Week 4 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 3.1 - 3.3 

### R5. Why is it that voice and video traffic is often sent over TCP rather than UDP  in today’s Internet? (Hint: The answer we are looking for has nothing to do  with TCP’s congestion-control mechanism.)
TCP provides a reliable data transer. It ensures the data is delivered from sending process to receiving process correctly and in order. Something like video traffic would only require one connection and would typically not support any loss of packets (loss of data), because of this, video and voice traffic uses TCP instead of UDP.

### R7 Suppose a process in Host C has a UDP socket with port number 6789.  Suppose both Host A and Host B each send a UDP segment to Host C with  destination port number 6789. Will both of these segments be directed to the  same socket at Host C? If so, how will the process at Host C know that these  two segments originated from two different hosts?
UDP includes 2 segments: destination IP address and destination port number. This means that if Host A & B are both sending packets to the same destinaion port number (to Host C), then the 2 will go to the same socket. Host C can extract the source port number (to determine which host it was sent from), the UDPServer.py uses recvfrom() method to extract the client-side (source) port number.

### R8. Suppose that a Web server runs in Host C on port 80. Suppose this Web  server uses persistent connections, and is currently receiving requests from  two different Hosts, A and B. Are all of the requests being sent through the  same socket at Host C? If they are being passed through different sockets, do  both of the sockets have port 80? Discuss and explain.
Persistent connections is a sign of TCP, and with TCP, the receiving sockets make a connection and create a socket that includes the source port number. So in this case, the requests being sent through will go to different sockets.

## Section 3.4

### R9. In our rdt protocols, why did we need to introduce sequence numbers?
When there is an error in the ACK or NAKs, if there is an error, it needs to repeat the last sent packet (as the original was lost). Sequence numbers are introduced in order to  recover potentially corrupted ACK or NAK packets. This way, we can tell if the receiver has received all of the packets it was supposed to.

### R10. In our rdt protocols, why did we need to introduce timers?
When the reciever must be resent a packet by the sender because a packet was lost, but sometimes the user does not know what was lost - the packet, the ACK or if they were delayed. Using a countdown timer will interrupt the sender after a specific time has expired.

### R11. Suppose that the roundtrip delay between sender and receiver is constant and  known to the sender. Would a timer still be necessary in protocol rdt 3.0,  assuming that packets can be lost? Explain.
Yes, because a long round-trip delay may wait a large amount of time depending on buffers and processing the packet must go through. In order to prevent duplicate data packets (where a packet is resent because it has taken too long to be receivied), we introduce timers.

## Section 3.5

### R15. Suppose Host A sends two TCP segments back to back to Host B over a  TCP connection. The first segment has sequence number 90; the second has  sequence number 110.  
#### a. How much data is in the first segment?
  There are 10 bits of data in the first segment
#### b. Suppose that the first segment is lost but the second segment arrives at  B. In the acknowledgment that Host B sends to Host A, what will be the  acknowledgment number?
  The acknowledgement for it being lost and resent will make the number 116. 

## Problems 

### P3. UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?
* The sum of the 1st 2 bytes would be: 
    01010011
  + 01100110
  -----------
    10111001
* The sum of the 1st 2 bytes & 3rd would be: 
    10111001
  + 01110100
  -----------
    00101110 *had overflow 1 that wrapped around
* Complement of sum: 11010001
* UPD checksum provides error detection. The complement result it put in the checksum field of the udp segment. This is because when the complement is added to the total sum of all bytes, it will equal 11111111. If there is a 0, you know there was an error. The wrap around addtion allows for the errors to not go undetected.
