# Week 4: The Transport Layer

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 4 Vegetables](Veggies.md)
* [Week 4 Learning Goals](Wk4_LearningGoals.md)
* [WireShark TCP Lab](Lab04_WireShark_TCP.md)

