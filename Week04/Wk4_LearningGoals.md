# Week 4 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain how TCP and UDP multiplex messages between processes using sockets.
* Demultiplexing - identifing the receiving sockets and delivering the data in a transport-layer segment to the correct socket
* Multiplexing - gathering the data chunks at the source host from different sockets, encapusulating each data chunk with header info to create segnments, and passing the segments to the network layer
* TCP socket is identified with four-tuple: source IP address, source port number, destination IP address, destination port number. Biggest difference with UDP is when 2 segments arrive with different source IP addresses or source port numbers, they will be directed to 2 different sockets. TCP has a welcoming socket to first establish connection. 
* UDP when created the transport later automatically assifns a port number to the socket. It is fully identifiable by a two-tuple: destination IP address and destination port number. So if 2 UDP segments have a different source but same destination,they will be directed to the same socket.


## I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.
UPD:
* connectionless transport, no handshaking between sending and receiving transport-layer entities
* Ex: DNS, remote file server, network management, name translation
* UDP will immediately package up the data and send it off vs TCP may throttle with its congestion-control mechanism
* no delays to establish connection
* 8 bytes of overhead

TCP:
* TCP has congestion control & will resend a segment until the receipient of the segment has been acknowledged by the destination 
* reliable 
* extra time to establish connection
* sends buffers 
* 20 bytes of overhead
* ex: electronic mail, remote terminal access, file transfer, streaming multimedia

## I can explain how and why the following mechanisms are used and which are used in TCP): sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.
* sequence numbers: used by TCP sender and receiver in implementing a reliable data transfer service. Follow a stream of transmitted bytes (not transmitted segments). Sequence number is byte-stream number of the first bute in the segment
* duplicate acks: used by TCP, it is an ACK that reacknowledges a segment for which the sender has already recived an earlier acknowledgement, for example: when an error/gap is detected in the data stream (aka a missing segment) it will reacknowledge the last in-order bute of data it has received
* timers: used to timeout//retransmit a packet, possibly bec the packet was lost within the channel, can occur when a packet is delayed but not lost (premature timeout), or when a packet has been recieved by the receiver but the reciever-to-snder ACK has been lost, duplicate copies of a packet may be recieved by a reciever, used by TCP
* pipleining: Allows multiple packets to be transmitted but not acknowledged - sender utilization can be increased over a stop-and-wait mode of operation, used by TCP
* go-back-N: used by TCP to allow the sender to trasnmit multiple packets (when available) without waiting for an acknowledgement, but is constrained to have no more than some maximum allowable number, *N*, of unacknowledged packets in the pipeline
* selective repeat: avoids unnecessary transmissions by having the sender retransmit only those packets that is suspects were received in error at the receiver. (requires that the receiver *individually* acknowledges correctly received packets
* sliding window: aka GBN protocol, because the range of permissable sequence numbers for transmitted but now yet acknowledged packets can be viewed as a window of size *N* over the tange of sequence numbers & as the protocol operates, the window slides forward over the sequence number space

## I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.
* dropped packets: detected by using sequence numbers, duplicate acks, and timers (explained above)
* out-of-order packets: mostly detected using sequence numbers, selctive repeat
* corrupted/dropped acks: GBN & selective repeat
* duplicate packets/acks: sequence number & timers
