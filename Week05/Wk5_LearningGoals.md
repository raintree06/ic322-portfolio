# Week 5 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain the problem that TCP congestion control is solving for, and how it solves that problem.
congestion control solves for excessive traffic on a network (which would cause too many people, too little resources, so processes are slow and some data is lost)
* end-to-end congestion control - no explicit support given to transport layer
* network assisted congestion control routers provide explicit feedback to the sender and/or reciever regarding the congestion state of the networks - routers provide explicit feedback
How TCP solves the issue:
* slow start: focuses on the amount of data the reciever can accept, and sends as allowed by reciever from sender
** slowly increases the amount sent (increases congestion window) everytime the reciever sends an acknowledgement
* fast recovery: when error is detected (lost packets), the congestion window is cut in half, thus slowing the amount sent in order to recover

## I can explain the problem that TCP flow control is solving for, and how it solves that problem.
TCP flow control service is provided to eliminate the possibility of the sender overflowing the receiver's buffer, it is a speed-matching service (matching the rate at which the sender is sending against the rate at which the receiver application is reading)

It is provided by having the sender maintain a variable called the receive window (used to give the sender an idea of how much free buffer space is available at the receiver)
