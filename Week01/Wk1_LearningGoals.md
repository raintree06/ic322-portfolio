# Week 1 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain the role that the network core plays vs the network edge.
* sit at the "edge" of the internet aka end system
  * ex: desktop computers, servers, mobile devices
  * aka hosts bec they run application programs
* core: packet switches & links that connect the internets end systems
* network edge = end systems such as computers, servers & mobile devices that run applications while network core = packet switches & links that connect the network edge together

## I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.
* access networks: network that physically connects an end system to the 1st router
* 2 most common: DSL & cable
* DSL: translates digitial data to high-frequency tones for transmission over telelphone wires, makes use of existing local telephone infrastructure
* cable: makes use of cable TV infrastructure, connects cable head to end to neighborhoof juctions which reaches individual houses, hybrid fiber coax, external device connects to ethernet, lower contracted dtaa rates, shared broadcast medium
* fiber-to-the-home: faster than DSL & cable, optical fiber path from CO to home
* wireless: high speed w/out installing cabeling from CO to home, beam-forming tech, data sent from base station to modem in home

## I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.
* Queueing: depends on previous packets sent
* transmission: length of packet/ rate (bits/s)
  * time to push entire packets into link of transmission router A to B
* propagation: once pushed to link (transmission), must propagate to router B
  * depends on physical mediom (2E8 m/s - 3E8 m/s)
  * distance between 2 routers / prop. speed of link
* transmission has nothing to do with the distance of routers

## I can describe the differences between packet-switched networks and circuit-switched networks.
* circuit-switched: resources needed (buffers, link, transmission rate) all are reserved (they provide for communication btwn end system)
* packet switched: resources are not reserved! used on demand, may have to wait in queue to access comm. link

## I can describe how to create multiple channels in a single medium using FDM and TDM.
* FDM: frequency of link divided up among the connections for length of connection (called bandwidth)
* TDM: time divided into frames of fixed duration, each frame divided up into fixed # of time slots
* ciruit switching wasteful bec idle networks doesnt allow for more connections. resources are alloted for that one everytime

## I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.
* internet service providers; network of packet switches & communitcation links
* ISPs must be connected: "network of networks"
* want to connect end systems so they can send packets to each other
* different tiers: customers are lower-tier ISPs and lower tier ISPs are customers to higher-tier ISPs
* competing global ISP that are internconnected (so those) with different services may contact eachother
* tier 1: global ISPs reach regional ISPs
* pops (points of prescence):group of one or more routers @ same location where customers ISP can connect to provider ISP (middle-man)
* peer: directly connect networks so neither pays another
* IXP: where multiple ISPs peer together
* content-provider netowrks (like google) try to connect direct-connect but lso connect to tier 1 ISPs

## I can explain how encapsulation is used to implement the layered model of the Internet.
* routers + link layers implenebt bottom layers
* each layer: application, transport, network link, physical all adds their own piece of information to the data to be sent
* the layers provide detail abt the data that is to be sent to make the process easier to get to the receiver
