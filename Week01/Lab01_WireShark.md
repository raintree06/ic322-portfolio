# Lab 1: Wireshark Intro

**Mowery, Sydney** | ***IC322 Networks***

## Intro to the the WireShark Intro Lab

* This lab focused on getting to know the packet sniffer, WireShark. It focused on seeing "protocols in action". During this lab, I learned how to understand and read the packet analyzer to see the packet content. This lab primarily dealt with the HTTP messages.
* Collaberation: none
* [WireShark Introduction Lab Instructions](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_Intro_v8.1.docx)

## Lab Questions:

### 1. Which of the following protocols are shown as appearing (i.e., are listed in the Wireshark “protocol” column) in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?
Protocols DNS, HTTP, TCP, and TLSv1.2 are all appearing in my trace file (they are listed in the Wireshark "protocol" column).

### 2. How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received? (By default, the value of the Time column in the packet-listing window is the amount of time, in seconds, since Wireshark tracing began. (If you want to display the Time field in time-of-day format, select the Wireshark View pull down menu, then select Time Display Format, then select Time-of-day.)
It took .023485 seconds from when the HTTP GET message was sent until the HTTP OK reply was received.

### 3. What is the Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu)? What is the Internet address of your computer or (if you are using the trace file) the computer that sent the HTTP GET message?
The Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu) is 128.119.245.12 and the Internet address of your computer is 10.25.133.39

### 4. Expand the information on the HTTP message in the Wireshark “Details of selected packet” window (see Figure 3 above) so you can see the fields in the HTTP GET request message. What type of Web browser issued the HTTP request? The answer is shown at the right end of the information following the “User-Agent:” field in the expanded HTTP message display. (Firefox, Safari, Microsoft Internet Edge, Other)
Mozilla/5.0 is the type of Web browser that issued the HTTP request. (The answer is shown at the right end of the information following the “User-Agent:” field in the expanded HTTP message display. [This field value in the HTTP message is how a web server learns what type of browser you are using.])

### 5. Expand the information on the Transmission Control Protocol for this packet in the Wireshark “Details of selected packet” window (see Figure 3 in the lab writeup) so you can see the fields in the TCP segment carrying the HTTP message. What is the destination port number (the number following “Dest Port:” for the TCP segment containing the HTTP request) to which this HTTP request is being sent?
The destination port number (the number following “Dest Port:” for the TCP segment containing the HTTP request) to which this HTTP request is being sent is Port 80.

### 6. Print the two HTTP messages (GET and OK) referred to in question 2 above. To do so, select Print from the Wireshark File command menu, and select the “Selected Packet Only” and “Print as displayed” radial buttons, and then click OK.
![Printed HTTP Messages](<Lab01_PrintedGET.pdf>)
