# Week 1 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 1.1

### R1. What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?

Interchangeable terms. End systems are connected together by a network of communication links and packet switches. What sends data to and from each other. Access the internet through ISPs. Internet applications run on end systems/hosts.
End systems include base stations, mobile computers, tablets, etc.

Yes, a web server is an end system. 


## Section 1.2

### R4. List 4 access technologies. Classify each one has home access, enterprise access, or wide-area access.

Digital Subscirber line (DSL) - home access

Cable - home access

3G - wide area access

Ethernet - enterprise access
   

## Section 1.3

### R11. Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2 respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay.)

Total end-to-end delay: L/R1 + L/R2

### R12. What advantage does a circuit-switched network have over packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

Circuit-switch has resources to provide for communication (buffers, link transmission rate) are reserved for the duration of the communication session between the end systems. Packet-switch does not reserve these and therefore a session uses the resources on demand, meaning it may have to wait for access to a communitcation link.

For FDM, each circuit continuously gets a fraction of the bandwidth. With TDM, each circuit gets all the bandwidth periodically during brief intervals of time (during the slots).   

### R13. Suppose users share a 2 Mbps link. Also suppose each usertransmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time.)

#### a. When ciruit switching is used, how many users can be supported?**
      
2 Mbps / 1 Mbps = 2 users

#### b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay befre the link if 2 or fewer users transmit at the same time?**
  
2 users requires 2 mbps. 2 Mbps is supported, so there will not "essentially" be a queue before the link

#### c. Find the probability that a given user is transmitting.**

Because the probability that they are simultaneously active is only 20%

#### d. Suppose there are 3 users. Find the probability that at any given time, all 3 users are transmitting simultaneously. Find the fraction of time during which the queue grows.**

probability? It would be less that 20%

### R14. Why will 2 ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

You pay for ISPs based on how much traffic it exchanges with the provider, so peering 2 ISPs together allows for traffic to pass through a direct connection rather than "unstream intermediaries" - cost less!

IXP earns money by being a meeting point where multiple ISPs can peer together. Typically ISPs, to directly communicate, would have to go upstream and connect to an upper tier ISP who would then connect back down to the destination ISP. This way would require the source ISP to pay extra money to the competing tier 1 and other upper tier ISPs. IXP basically cuts off all those extra 3rd party upper tiers and allows tiers to connect to one another. It allows ISPs at the same tier to connect to multiple other ISPs. IXP providers can then charge the ISPs using the IXP money for the usage, as it provides the ISPs direct connection to same tier ISPs and is more of value use than having to go upstream to upper tiers.


## Section 1.4

### R18. How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5 times 10^8 m/s, and transmission rate 2 Mbps? More generally, how long des it take a packet of length L to propagate over a link of distance d, propagation speed s, and transmission rate R bps? Does this delay depend on a packet length? Does this delay depend on transmission rate?

L/R = transmission delay

d/s = propagation delay

2500km / 2.5*10^8 m/s = .01 sec

### R19. Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has 3 links, of rates. R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.

#### a. Assuming no other traffic in the network, what is the throughput for the file transfer?

R1 < (R2 & R3)

500 kbps

#### b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?

4 million bytes / (500 KBPS/3) = 24 seconds

#### c. Repeat (a) and (b) but now with R2 reduced to 100 kbps?

(a) 100 kbps

(b) 120 seconds
