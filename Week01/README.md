# Week 1: The Internet

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 1 Vegetables](Veggies.md)
* [Week 1 Learning Goals](Wk1_LearningGoals.md)
* [WireShark Introduction Lab](Lab01_WireShark.md)
