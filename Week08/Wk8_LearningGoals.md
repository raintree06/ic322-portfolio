# Week 8 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain the problem that NAT solves, and how it solves that problem.
* NAT is another way to help with address allocation, especially with SOHO (home office subnets)
* The issue is that for a **private network** or a **realm with private addresses**, there may be use of a specific address space that, within the private network, everything is sent fine, because it is recognizeable within that specific private network
  * however, other private networks may use that same address! (And everything works fine within that different private network too!
  * So if the private interface using the private network ip address wants to send something up more globally, the bigger network (who may have multiple private networks using the same IP address) will get confused on who the sender was!
  * private network addresses only have meaning within that private network
  * issue:

* NAT-enabled router:
  * doesn't look like a regular router - behaves to the outside world as a single device with a single IP address
  * it hides the details of the home network from the outside world
  * has one IP address for the entire home router
  * If all packets come in with the same address for the private network, how do you know which specific address within the private network it should be sent to?
    * contains a **NAT translation table** to determine which private network IP address an incoming packet is to be sent
    * contains port numbers & IP addresses
  * process:
    * private network user sends datagram to LAN. This private network user has a specific port and ip address
    * the datagram goes through the NAT-router first, where it is given the WAN-side IP address specific to the entire private network as is source IP address and a new source port number
      * the NAT-router can generate any port number not currently being used in NAT translation table
    * the NAT-router stores the datagrams original IP address and port number into a table with the corresponding IP address & port it was changed to.
    * the datagram is recieved by the host and a datagram is sent back to the ip address source
    * the NAT-router receives a datagram, compares the values with those in the NAT translation table and adjusts the ip destination address and destination port with the corresponding values
  * ![NAT Process](<NAT.png>)

## I can explain important differences between IPv4 and IPv6.
* 32-bit IPv4 address space was being used up super fast, so needed a bigger address space: IPv6

| Topic | IPv4 | IPv6 |
| ----- | ----- | ----- |
| IP address size| 32 bits | 128 bits |
| New Address | .. | Anycast Address |
| Header Size | 20-60 bytes | fixed 40 bytes |
| Flow | .. | specifically labelled |
| Datagram too big to be forwarded to outgoing link | fragmantation or reassembly | drops it & sends error to sender |
| checksum | header contains TTL field, so checksum must recompute at every router | does not include|
| options | within standard IP header | one of the possible next headers pointed to from within IPv6 header|

* IPv6 changes:
  * *Expanded addressing capabilities*: increases the size of IP addresses from 32 bits to 128 bits
    * world won't run out of Ip addresses!
    * new **anycast address**: allows datagram to be delivered to any one of a group of hosts
      * ex: can send an HTTP GET to the nearest number of mirror sites that contain a given document
  * *streamlined 40-byte header*: 40-byte fixed geader length allows for faster processing of the IP datagram by a router
    * IPv4 fields were either dropped or made optional
  * *Flow labeling*
    * flow allows the "label of packets belonging to particular flows for which the sender requests special handling, such as a non-default quality of service or real-time service
      * Ex: audio & video may be treated as flow
    * **flow is for packets that need specific handling**
    * eventual need to be able to differentiate among the flows

* fields no longer in IPv6:
  * *fragmentation/reassembly*: operations can only be performed by source & destination
    * too time consuming - speeds up network to remove it   
    * if IPv6 datagram received by router is too large to be forwarded to the outgoinf link, the router simply drops it & sends a "packet too big" ICMP error message back to the sender
  * *Header checksum*
    * transport-layer & link-layer protocols perform checksum, so it is redundant
  * *options*:
    * IPv4 contained within standard IP header
    * IPv6 has as optional header

* IPv6 datagram format:
  * ![IPv6 datagram format](IPv6.png)

## I can explain how IPv6 datagrams can be sent over networks that only support IPv4.

* **Tunneling**
  * the IPv4 routers that IPv6 routers surronding it and must send datagrams through are refered to as a **tunnel**
  * IPv6 node on sending side of tunnel takes IPv6 datagram & puts it in the data (payload) field of an IPv4 datagram
  * IPv4 routers route it through
  * the IPv6 receiving router determines the IPv4 datagram contains a IPv6 datagram through observing the protocol number field in the IPv4 datagram is 41
  * IPv6 router then extracts the IPv6 datagram and continues routing it
  * ![tunneling](tunneling.png)
