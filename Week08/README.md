# Week 8: Network Layer: Data Plane (Part 2)

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 8 Vegetables](Veggies.md)
* [Week 8 Learning Goals](Wk8_LearningGoals.md)
* [Week 8 Deep Dive Lab](Networks_Week_8_Lab.pdf)
* [Week 8 Peer Review](https://gitlab.com/usna-ic322/ic322-portfolio/-/issues/20)
