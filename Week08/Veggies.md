# Week 8 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 4.3

### R23. Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP address of its local DNS server. List these values.
Wireless LAn adapter Wifi:
* IP address: 10.25.133.39 (IPv4)
* Network Mask: 255.255.192.0
* Default Router: 10.25.128.1
* IP address of Local DNS Server: 10.1.74.10

### R26. Suppose you purchase a wireless router and connect it to your cable modem. Also suppose that your ISP dynamically assigns your connected device (that is, your wireless router) one IP address. Also suppose that you have five PCs at home that use 802.11 to wirelessly connect to your wireless router. How are IP addresses assigned to the five PCs? Does the wireless router use NAT? Why or why not?
* IP addresses assigned using DHCP
* DHCP assigns addresses automatically to the interfaces on the network
* the router uses NAT because it is assigned one IP address from the ISP rather than a slot of IP addresses to break apart for each interface in the private network

### R29. What is a private network address? Should a datagram with a private network address ever be present in the larger public Internet? Explain.
* a private network address is an address on a private network that uses a NAT to communicate with the gloabl network
* it has a network address identical to other private networks, and one IP address that the NAT table uses to assign the outgoing datagrams from the private network
* this IP address is also used for incoming datagrams addressed to a private network interface
* no, the private network address will not be present because there are other private networks with the same address
* the specific port number with the global IP address for a private network will be the main identifier to tell which private network interface the datagram is being sent/sending too

## Problems

### P15. Consider the topology shown in Figure 4.20. Denote the three subnets with hosts (starting clockwise at 12:00) as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F.
#### a. Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254/23; Subnet A should have enough addresses to support 250 interfaces; Subnet B should have enough addresses to support 120 interfaces; and Subnet C should have enough addresses to support 120 interfaces. Of course, subnets D, E and F should each be able to support two interfaces. For each subnet, the assignment should take the form a.b.c.d/x or a.b.c.d/x – e.f.g.h/y.
* 214.97.254 = 11010110 01100001 11111110 00000000
  * only /23 bits matter, so the last **0 00000000** don't matter
* A: 2^8 = 256, so it needs /31 to use
* B: 2^7 = 126, so it needs /30 to use
* C: 2^7 = 126, so it needs /30 to use
* D: 2^1 = 2, so it needs /24 to use
* E: 2^1 = 2, so it needs /24 to use
* F: 2^1 = 2, so it needs /24 to use + 1 because the other 2 already in use: so /25

* 11010110 01100001 1111111- --------
* Subnet A: - -------0
* Subnet B: - ------00
* Subnet C: - ------00
* Subnet D: - -0000000
* Subnet E: - -0000000
* Subnet F: - -0000000

#### b. Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers.

|Subnet|IP Address|
|----|----|
|A|11010110 01100001 1111111**0 1110000**0
|B|11010110 01100001 1111111**1 100000**00
|C|11010110 01100001 1111111**0 110000**00
|D|11010110 01100001 1111111**0** 00000000
|E|11010110 01100001 1111111**1** 00000000
|F|11010110 01100001 1111111**0 1**0000000

### P16. Use the whois service at the American Registry for Internet Numbers (http://www.arin.net/whois) to determine the IP address blocks for three universities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use www.maxmind.com to determine the locations of the Web servers at each of these universities.
[https://www.arin.net/whois](https://www.arin.net/whois)
[https://www.maxmind.com/en/home](https://www.maxmind.com/en/home)

* UVA: 128.143.0.0 - 128.143.255.255
  * Charlottesville, VA: 38.0321,-78.489   
* MIT: 129.55.0.0 - 129.55.255.255
  * Lincoln Laboraytory, Lexington, MA: 37.751,-97.822
* George Mason University: 2620:10e:6000:: - 2620:10e:60ff:ffff:ffff:ffff:ffff:ffff
  * Fairfax, VA: 38.8476,-77.3281

* whois services gives the address of where the organization home is setup
* maxmind gives the exact coordinates of the range of where the network is

### P18. Consider the network setup in Figure 4.25. Suppose that the ISP instead assigns the router the address 24.34.112.235 and that the network address of the home network is 192.168.1/24.
#### a. Assign addresses to all interfaces in the home network.
#### b. Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table.

![P18 Answer](<P18.jpg>)

### P19. Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the outside world.

#### a. Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behind a NAT? Justify your answer.
#### b. If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer.
Count the changes in port numbers

