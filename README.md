# IC322 Portfolio

**Mowery, Sydney** | ***Fall 2023***

This is my Portfolio for Networks Class!

* **[Week 1: The Internet](/Week01/)**
    * [Week 1 Learning Objectives](/Week01/Wk1_LearningGoals.md)
* **[Week 2: The Application Later: HTTP](/Week02/)**
    * [Week 2 Learning Objectives](/Week02/Wk2_LearningGoals.md)
* **[Week 3: The Application Later: DNS](/Week03/)**
    * [Week 3 Learning Objectives](/Week03/Wk3_LearningGoals.md)
* **[Week 4: The Transport Layer](/Week04/)**
    * [Week 4 Learning Objectives](/Week04/Wk4_LearningGoals.md)
* **[Week 5: The Transport Layer (continued)](/Week05/)**
    * [Week 5 Learning Objectives](/Week05/Wk5_LearningGoals.md)
* **[Week 7: Network Layer: Data Plane](/Week07/)**
    * [Week 7 Learning Objectives](/Week07/Wk7_LearningGoals.md)
* **[Week 8: Network Layer: Data Plane (continued)](/Week08/)**
    * [Week 8 Learning Objectives](/Week08/Wk8_LearningGoals.md)
* **[Week 9: Network Layer: Control Plane](/Week09/)**
    * [Week 9 Learning Objectives](/Week09/wk9LG.md)
* **[Week 10: Network Layer: Control Plane (continued)](/Week10/)**
    * [Week 10 Learning Objectives](/Week10/Wk10_LearningGoals.md)
* **[Week 12: Link Layer](/Week12/)**
    * [Week 12 Learning Objectives](/Week12/wk12LG.md)
* **[Week 13: Link Layer (continued)](/Week13/)**
    * [Week 13 Learning Objectives](/Week13/wk13LG.md)
* **[Week 15: TLS & Network Security](/Week15/)**
    * [Week 15 Learning Objectives](/Week15/wk15LG.md)
