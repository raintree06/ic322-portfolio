# Week 2 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 2.1

### R5: What information is used by a process running on one host to identify a process running on another host?
For one process to send packets to a process running on another host, the address of the host and an identifier of the receiving process in the destination host (called the IP address) is needed.

### R8: List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.
1. Reliable Data Transfer: guaranteed data delivery service
    * TCP only 
2. Throughput: guaranteed rate of connection
    * not guaranteed by either
3. Timing
    * not guaranteed by either
4. Security: encryption of data
    * TCP


## Section 2.2 - 2.5

### R11: Why do HTTP, SMTP, and IMAP run on top of TCP rather than UDP?
TCP provides a reliable data trander service. TCP recovers from loss or reordering of data within the network, unlike UDP. TCP can provide a direct connection without errors.

### R12: Consider an e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies.
Servers users visit can assign users unique ID numbers that are stored in the servers "back-end database". So when a user requests something from a specific server, the server will respond with a "Set-coookie" header that contains the ID-number. This ID number is kept on the user's end system and managed by the user's browser.

### R13: Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?
When a user tells the browser to request an object, the first thing that happens is the browser establishes a connection to the Web cache. The cache is a place where recently requested objects are stored. So when the browser makes a connection, the web cache checks to see if it has a copy of the requested object stored locally, if it doesn't the web cache opens a TCP connection to the origin server to request the object the user is looking for. The cache then stores a copt in its local storage. It can reduce the delay in receiving an object because it allows for reduced traffic on an institution's website. If the bottleneck bandwidth between the client and cache is less than the client and the origin, it makes the time to receive the requested object much faster. However, if the object is not within the cache (user has not requested the same object before), then the cache will not reduce the delay.

### R14: Telnet into a Web server and send a multiline request message. Include in the request message the If-modified-since: header line to force a response message with the 304 Not Modified status code.
  GET /wireshark-labs/HTTP-wireshark-file2.html HTTP/1.1\r\n
  Host: gaia.cs.umass.edu\r\n
  User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0\r\n
  Accept: text/html, application/ ...
  Accept-Language: en-US,en;q=0.5\r\n
  Accept-Encoding: gzip, deflate\r\n
  Connection: keep-alive\r\n
  Upgrade-Insecure-REquests: 1\r\n
  If-Modified-Since: Sun, 17 Sep 2023 05:59:01 GMT\r\n
  If-None_Match: "173-..."\r\n
  ...


## Section 2.6

### R26: In section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support simultaneous connections, each from a different client host, how many sockets would the TCP server need?
UDP works by having the destination address attach to the packet and then pass through the sender's socket. The Internet then uses the destination address to route the packet through the internet to the socket in the receiving process. So it only takes one socket to send the packet, and then it can be receiveid by whatever specific socket it is directed to for the correct action to be taken. So essentially once the sender socket sends off the packet, the socket is no longer in use.

The first thing TCP does is establish a connection between the client socket and the server socket. So through TCP the packet can be dropped through the connection, with no destination address connected because the connection between the sockets are already established (versus UDP where there is no connection). If a server wants to support multiple connections, it will need one socket for each individual connection (because the server socket must directly connect to the client socket).
