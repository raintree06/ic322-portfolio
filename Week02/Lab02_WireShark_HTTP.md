# Lab 2: Wireshark HTTP

**Mowery, Sydney** | ***IC322 Networks***

## Intro to the the WireShark HTTP Lab

* This lab focuses on the HTTP protocols such as the "basic GET and response interation, HTTP message formats, retrieving large HTML files, and retrieving HTML files with embedded objects, and HTTP authentication and security". In this lab, I learned about how to cache is used to store recently visited sites and the difference between different HTTP GET responses.
* Collaberation: none
* [WireShark HTTP Lab Instructions](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_HTTP_v8.1.doc)

## PART 1: The Basic HTTP GET/Response Interaction

### 1. Is your browser running HTTP version 1.0, 1.1, or 2? What version of HTTP is your server running?
My browser is running HTTP version 1.1. My server is running version 1.1.

### 2. What languages (if any) does your browser indicate that it can accept to the server?
My browser indicates that it can accept "en-US, en", meaning US English or any English.

### 3. What is the IP address of your computer?  What is the IP address of the gaia.cs.umass.edu server?
The IP address of my computer is 10.25.133.39. The IP address of the gaia.cs.umass.edu server is 128.119.245.12.

### 4. What is the status code returned from the server to your browser?
The status code returned from the server to my browser is "200 OK". This means, as per the textbook, "request succeeded and information is returned in response".

### 5. When was the HTML file that you are retrieving last modified at the server?
The HTML file I am retrieving was last modified Thursday, 07SEP2023 05:59:02 GMT.

### 6. How many bytes of content are being returned to your browser?
There are 128 bytes of content being returned to my browser.

### 7. By inspecting the raw data in the packet content window, do you see any headers within the data that are not displayed in the packet-listing window?  If so, name one.
I do not see any headers within the data that are not displayed in the packet-listing window.


## PART 2: The HTTP CONDITIONAL GET/Response Interaction

### 8. Inspect the contents of the first HTTP GET request from your browser to the server.  Do you see an “IF-MODIFIED-SINCE” line in the HTTP GET?
I do not see an "IF-MODIFIED-SINCE" line in the first HTTP GET request.

### 9. Inspect the contents of the server response. Did the server explicitly return the contents of the file?   How can you tell?
The server explicitly returned the contents of the file under the "Line-based text data: text/html (10 lines)" header. I can tell because it has the contents of what was said in the browser under this header.

### 10. Now inspect the contents of the second HTTP GET request from your browser to the server.  Do you see an “IF-MODIFIED-SINCE:” line in the HTTP GET1? If so, what information follows the “IF-MODIFIED-SINCE:” header?
There is an “IF-MODIFIED-SINCE:” line in the 2nd HTTP GET. The information that follows the header is the date & time: "Thu, 07 Sep 2023 05:59:02 GMT".

### 11. What is the HTTP status code and phrase returned from the server in response to this second HTTP GET?  Did the server explicitly return the contents of the file?   Explain.
The HTTP status code and phrase in the response to the 2nd HTTP GET request was "304 Not Modified". The server did not explicitly return the contents of this file.

This is because the request has previously been made and was stored and retrieved from the cache. This means that when the cache is checked and if there is the same file that is requested within the cache, a "last modified since" check will be made. The date will be the date of the first search of the file to see if changes have been made since the first search till now (when the most recent get request of the same file is made).


## PART 3: Retrieving Long Documents

### 12. How many HTTP GET request messages did your browser send? Which packet number in the trace contains the GET message for the Bill of Rights?
My browser sent only 1 GET resquest message. The packet that contained the GET message for the Bill of Rights was 30.

### 13. Which packet number in the trace contains the status code and phrase associated with the response to the HTTP GET request?
The packet number in the trace that contains the status code and phrase associated with the response to the HTTP GET request is packet number 36. (Status code and phrase was "200 OK".)

### 14. What is the status code and phrase in the response?
The status code and phrase in the response was "200 OK".

### 15. How many data-containing TCP segments were needed to carry the single HTTP response and the text of the Bill of Rights?
There were 5 data-containing TCP segments needed to carry the single HTTP response and the text of the Bill of Rights.


## PART 4: HTML Documents with Embedded Objects

### 16. How many HTTP GET request messages did your browser send?  To which Internet addresses were these GET requests sent?
My browser sent 2 HTTP GET rewuest messages. Both GET requests were sent to Internet address 10.25.133.39.

### 17. Can you tell whether your browser downloaded the two images serially, or whether they were downloaded from the two web sites in parallel?  Explain.
The two images were downloaded serially rather than parallel. The first sign of this was that the 2nd photo loaded in after the other information on the page was already in place. Looking at the WireShark page, the responses were made seperately, one after the other rather than combined. The HTTP responses to each seperate GET request were made at different times.


## PART 5:

### 18. What is the server’s response (status code and phrase) in response to the initial HTTP GET message from your browser?
The server's response (status code & phrase) in the response to the intial HTTP GET response from my browser was "401 unauthorized".

### 19. When your browser’s sends the HTTP GET message for the second time, what new field is included in the HTTP GET message? 
When my browser sends the HTTP GET request for the 2nd time, it includes the new field "Authorization: Basic" followed by more chracters.

