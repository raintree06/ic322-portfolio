# Week 2 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain the HTTP message format, including the common fields.
* Request:
  * GET /somepage.html HTTP/1.1
    Host: www.whatever.edu
    Connection: close
    User-agent: Mozilla/5.0
    Accept-language: fr
    entity body {}
  * request line is 1st line
    * 3 parts/fields: method, URL, HTTP version
      * method: GET, POST, HEAD, PUT, DELETE
        * get used when browser requests an object
      * version: HTTP/1.1 in example
  * header lines follow 1st line (request line)
    * host: specifies host
    * connection: close says close connection after requested object is sent (send it then done)
    * user-agent: browser type making request
    * accept lang: language versuon of object (otherwise default)
* response:
  * ex: http/1.1
    connection: close
    date: tues 18 aug 2015 15:44:04 GMT
    server: apache/2.2.3 (centos)
    last modified: ---
    content length: 6841
    content-type: text/html
    ...
  * 3 sections: initial status line, 6 headers, entity body
  * status line: 3 fields, protocol version, status code, status message

## I can explain the difference between HTTP GET and POST requests and why you would use each.
* both request message parts
* post uses entity body
  * used for ex, when user fills out a form-like entering search in search engine
  * specific contents on page dependent user
  * body = what user entered
* GET is full URL, which HTML forms often use

## I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.
* verifies the object user is trying to 'get' is up-to-date
* includes if-modified-since header line
* if an object is stored locally in cache when user requests object again, cache must perform up-to-date check
* condititional get contains "if modified since" date when object 1st went to the cache
* GET
  HOST www...
  If-Modified-Since ...

## I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.
* response code = response message to the request the server just received a request from
* common status codes:
  * 200: OK
  * 301: moved permanently (URL moved -> gets new URL)
  * 400: BAD REQUESTS: request not understoof
  * 404: Not Found: requested doc DNE on server
  * 505: HTTP version not supported (by server)

## I can explain what cookies are used for and how they are implemented.
* allows sutes to keep track of users
* server creates unique ID number when user 1st visits site, then respons to user w/ ID #
* browser (of a user) keep users "cookie file" w/ different website's ID # for the user

## I can explain the significance HTTP pipelining.
* TCP connection is left open by the server
* allows for the "subsequent" requests & responses between client + server to be sent over same connection
* requests are made back-to-back w/out waiting for replies to pending requests

## I can explain the significance of CDNs, and how they work.
* when the request for objects is made, browser TCP connects w/ Web cache to see if it is stored locally (recently requested),if not, Web cache opens TCP connection w/ orgin server to request object. Then stores it in the cache
* cache's help reduce traffic on institutions access link
* CDNs (content distribution networks) instals many caches in internet to localize traffic
* typically deployed in ISPs all over world
