# Week 2: The Application Layer: HTTP

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 2 Vegetables](Veggies.md)
* [Week 2 Learning Goals](Wk2_LearningGoals.md)
* [WireShark HTTP Lab](Lab02_WireShark_HTTP.md)

