# Week 13 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can describe the role a switch plays in a computer network.
- Switches role is to receive incoming link-layer frames and forward them onto outgoing links
- it is transparent to the hosts and routers in the subnet
- rate at which frames arrive to any one of the switch’s output interfaces may temporarily exceed the link capacity of that interface
- Switches Forward & Filter using a switch table

## I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.
- Each interface/router has network-layer addresses & link-layer addresses. ARP table helps translate between the two
- ARP table **stores both the MAC addresses & the IP addresses** of devices on the **same subnet**
- ARP Table stores the addresses & the Time-To-Live value (when a set of addresses will expire in a table - typically ~20 minutes)
- If source can not find its destination in the ARP table, it will send out an ARP query packet to all hosts & routers - if a host/router matches the query address, it responds

## I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them.
- each station senses the channel before transmitting, and refrains from transmitting when the channel is sensed busy
- CSMA/CA = Carrier Sense Multiple Access with Collision Avoidance
    - Used by MAC protocols
    - uses link-layer acknowledgement/retransmission (ARQ) scheme
- CSMA/CD = CSMA with Collision Detection
    - used by Ethernet
- MAC does not use collision detection because:
    - to detecting collisions requires ability to send and receive at the same time (very costly to build hardware that can detect this)
    - adapter could not detect all collisions due to hidden terminal problem and fading
- when the destination station receives a frame that passes the CRC, it waits a short period of time known as the Short Inter-frame Spacing (SIFS) and then sends back an acknowledgment frame. If the transmitting station does not receive an acknowledgment within a given amount of time, it assumes that an error has occurred and retransmits the frame, using the CSMA/CA protocol to access the channel. If an acknowledgment is not received after some fixed number of retransmissions, the transmitting station gives up and discards the frame
