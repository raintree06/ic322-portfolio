# Week 13 VintMania

**Mowery, Sydney** | ***IC322 - Networks***

## 1. Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?
    Vint Cerf co designed the TCP/IP protocols and aritechture of the internet

## 2. Find 3 surprising Vint Cerf facts.
1. In 2018, Cerf was named a recipient of the Benjamin Franklin Medal
2. In 1965 Cerf received a bachelor’s degree in mathematics from Stanford University in California.
3. Received the Presidential Medal of Freedom, the highest civilian award of the United States.

## 3. Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.
1. How does it feel seeing the work that you put in expand beyond anything you were thinking back then? What is it like to be the one who helped create everything we have now?
2. What was the goal behind your original desire to be a computer scientist?