# Week 13 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Chapter 6, Section 6.4

### R10. Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?
C's adapter will process the frames to see if the destination address matches its own address. Because it does not, C's adapter will discard the frames.
If sent withe the broadcast address, C would react the same as described above except it would them send the frames to the network later C instead of discarding the frames.

### R11. Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?
ARP query is meant to query all hosts and royters on a subnet to determine MAC address corresponding to the IP address it is looking for. It is sent within a broadcast frame because the request needs to be processes by all adapters. The response is sent within a specific destination MAC address frame because the sender is meant to specific the address to the ARP Table.

## Chapter 6 Problems

### P14. Consider three LANs interconnected by two routers, as shown in Figure 6.33.
![p14 Image](p14.png)

#### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.
Subnet 1:
- A: 192.168.1.2
- B: 192.168.1.3
Subnet 2:
- C: 192.168.2.2
- D: 192.168.2.3
Subnet 3:
- E: 192.168.3.2
- F: 192.168.3.3

#### b. Assign MAC addresses to all of the adapters.
Adapter 1:
192.168.1.1

Adapter 2:
192.168.2.1

Adpter 3:
192.168.2.4

Adapter 4:
192.168.3.1

#### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.
1. source checks for the IP address & MAC address in the ARP table for the specified destination
2. Once it knows the IP and MAC address, the source gives the adapter these items, which constructs a link-layer frame containing the destination's MAC address and send the frame into the LAN

#### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).
1. source passes ARP query packet to the adapter along with an indication that the adapter should send the packet to the MAC broadcast specififed
2. adapter encapsulates he ARP packet ina link-layer frame, uses the broadcast address for the frame's destination address, and trasnmits the frame into the subnet
3. all adapters on the subnet receive the frame containing the ARP query and each adapter passes the ARP packet within the frame up to its ARP module (each arp modules check to see if the IP address matches the destination address in the ARP packet)
4. when one matches, it respons back to the hst a response ARP packetwith the desired mapping
5. source updates ARP table and sends its IP datagram encapsulated in a link=layer frame whose destination is the MAC address of that who responded to the query

### P15. Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.
![p14 Image](p14.png)

#### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?
Host E will not ask R1 to help because F is within the same subnet! The next answer will be the IP and MAC addresses of E & F, given what they were assigned in a previous question.

#### b. Suppose E would like to send an IP datagram to B, and assume that E’s ARP cache does not contain B’s MAC address. Will E perform an ARP query to find B’s MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination IP and MAC addresses?
Yes! E will perform an ARP query to find the correct destination address of B because it needs to figure this out in order to send the frame.

#### c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?
The switch will process the request and respond. R1 will not receive the ARP request because S1 will respond before then.

## Chapter 7, Section 7.2

### R3. What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?
- path loss
    - decreasing signal strength
    - signal will dispurse as distance between sender and recevier increases
- multipath progagation
    - occurs when portions of the electromagnetic wave reflect off objects and the ground, taking paths of different lengths between a sender and receiver
    - results in blurring of received signal at the receiver
    - moving objects between the sender and receiver can cause multipath propagation to change over time
- interference from other sources:
    - radio sources transmitting will interfere with eachother
    - electromagnetic noise within the environment (e.g., a nearby motor, a microwave) can result in interference

### R4. As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?
Increase SNR by increasing transmission power.
​For a given SNR, a modulation technique with a higher bit transmission rate (whether in error or not) will have a higher BER.
Dynamic selection of the physical-layer modulation technique can be used to adapt the modulation technique to channel conditions.

## Chapter 7 Problems

### P6. In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?
The idea behind this may have been that all frames waiting for idle time may transmit as soon as it is not idle again, thus resulting in more collisions. Waiting for the 2nd idle frame would mean it would have a chance to detect if anothe frame was sent and if it should wait again to avoid collision.

### P7. Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment.
The total time of transmitting that many bits of data would come from the time to transmit its data frame and the time to trasnmit an acknolowdgement.