# Week 9: Network Layer: Control Plane

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 9 Vegetables](Veggies.md)
* [Week 9 Learning Goals](wk9LG.md)
* [Week 9 Deep Dive Lab](Networks_Week_9_Lab.pdf)
* [Week 9 Peer Review](https://gitlab.usna.edu/ndzayfman/ic322/-/issues/22)
