# Week 9 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 5.2

### R5. What is the “count to infinity” problem in distance vector routing?
* the count to infinity error occurs when there is issues with updating the costs of getting from one destination to another
* occurs when a cost changes but the updates have not sent out yet, and so packets will nounce back and forth between a router as it looks for the shortest path

## Section 5.3 - 5.4

### R8. True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.
* False
* The information for OSPF is shared among the entire autonomous system

### R9. What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?
* an area of an AS is a hierarchical way to organize an AS
* each area has its own OSPF link-state routing algorithm
* one area in an AS is the backbone that routes traffic between the areas of the AS

## Problems

### P3. Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.​
![p3 pic](<p3prob.png>)

* ![p3](<p3.PNG>)

### P4. Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following:
#### a. Compute the shortest path from v to all network nodes.
* ![p4a](<p4v.PNG>)

#### b. Compute the shortest path from y to all network nodes.
* ![p4b](<p4y.PNG>)

#### c. Compute the shortest path from w to all network nodes.
* ![p4c](<p4w.PNG>)

### P5. Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z.
![p5prob](<p5prob.png>)

| |z|v|x|u|y|
|--|--|--|--|--|--|
|**z**|**0**|**5**|**2**|**6**|**5**|
|**v**|6|0|3|1| |
|**x**|2|3|0| |3|
|**u**| |1| |0|2|
|**y**| | |3|2|0|

### P11. Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: , , , , . Suppose that poisoned reverse is used in the distance-vector routing algorithm.
![p11prob](<p11prob.png>)
#### a. When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?


#### b. Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.


#### c. How do you modify c(y, z) such that there is no count-to-infinity problem at all if c(y, x) changes from 4 to 60?
