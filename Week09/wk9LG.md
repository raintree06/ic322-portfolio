# Week 9 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.
* Link Algorithms are routing algorithms where the network topology and all link costs are known, available as input to the LS algorithm
* Accomplished by having each node broadcast link-state packets to all other nodes in the network
  * link-state packets contain the identities and costs of its attached links
    * done with link-state broadcast algorithm
* All nodes have identical and complete view of the network
* Each Node can run LS algorithm & compute the same set of least-cost paths as every other node

* Main LS Algorithm is **Dijkstra's Algorithm**
  * Computes the least-cost path from one node to all other nodes in the network
  * Consists of initialization step followed by a loop
    * number of times loop is executed = # of nodes in the network
  * *D(V)* = cost of the least-cost path from the source node to destination v as of this iteration of the algorithm
  * *p(v)* =  previous node (neighbor of v) along the current least-cost path from the source to v
  * *N’* = subset of nodes; v is in N’ if the least-cost path from the source to v is definitively known
![Link-State Algorithm Example](<figure.png>)
![Link-State Algorithm Example](<LS.png>)

* Issues:
  * Oscillations: when a router continuously detects more efficient paths (such as when the link cost depends on the load carried)
* Solutions to Issues:
  1. Mandate that link costs not depend on the amount of traffic it carries
    * Not great because one goal of routing is to void highly congested links
  2. Ensure that not all routers run the LS algorithm at the same time
    * Hope that if each router ran the LS algorithm, it would not be the same at each node
      * Routers can self-synchronize
        * Avoid this by having each router randomize the time it sends out to a link advertisement

* Protocol based on the LS Algorithm:
  * Internet’s OSPF routing protocol


## I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.
* decentralized routing algorithm
* each node maintains a vector estimate of the costs (distances) to all other nodes in the network
* The DV algorithm is:
  * *distributed* bec each node receives some info from one or more of its directly attached neighbors, performs a calculation, and distributes the results back to all of its neighbors
  * *iterative* bec it continues this process until there is no more info being exchanged between neighbors
  * *asynchronous* bec it does not require all the nodes to operate in lockstep with each other
* uses the Bellman-Ford equation to provide entries in node *x*'s forwarding table

* Basically what happens is:
  1. each node takes in the information that is knows from its directly attached neighbors (the cost to get to the neighbor)
  2. the nodes exchange information of their own neighbors with their neighbors (so now nodes may know the cost from their neighbor to their neighbor's neighbor)
  3. the nodes update their own costs and connections given their neighbor's information
  4. because of the update to their own information, the node will then exchange their own updated information to their neighbors again
  5. process continues until there is no more updates to share

![DV algorithm in operation](<DV.png>)

* Issues:
  * can cause **routing loops**: Occurs when the tables in each router read that the last sent router has a shorter path to the destination, and will request info back and forth between two nodes forever or until forwarding tables are change
    * happens when tables have not updated yet

* Protocol based on the DV Algorithm:
  * DV-like algorithms are used in many routing protocols in practice, including the Internet’s RIP and BGP, ISO IDRP, Novell IPX, and the original ARPAnet

## 
![LS vs DV algorithms](<LSvsDV.PNG>)
