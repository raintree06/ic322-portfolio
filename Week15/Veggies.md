# Week 15 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Chapter 8, Section 8.5 - 8.8

### R20. In the TLS record, there is a field for TLS sequence numbers. True or false?
False, it is incorporated into the HMAC calculation!

### R21. What is the purpose of the random nonces in the TLS handshake?
Nonce is a number that a protocol will use only once in a lifetime. That is, once a protocol uses a nonce, it will never use that number again. Within the TLS handshake, the nonce is used to create session keys.

### R22. Suppose an TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear.
True. Step one of CBC operations: Before encrypting the message (or the stream of data), the sender generates a random k-bit string, called the Initialization Vector (IV). Denote this initialization vector by c(0). **The sender sends the IV to the receiver in cleartext**.

### R23. Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?
Bob will find this out in step 2 of the TCP handshake. As soon as the certificate is sent, Bob uses CA to check the certificate. This happens before a master key is created!

## Chapter 8 Problems

### P9. In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, Sa and Sb, respectively. Alice then computes her public key, Ta, by raising g to Sa and then taking mod p. Bob similarly computes his own public key Tb by raising g to Sb and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising Tb to Sa and then taking mod p. Similarly, Bob calculates the shared key S' by raising Ta to Sb and then taking mod p. 

#### a. Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S'.
public:
- p = 109 (large prime number)
- g = 79 (large number, less than p)
private:
- Sa = 6 (Alice secret key)
- Sb = 7 (Bob secret key)
creating public keys:
- Ta = g^(Sa) % p = 79^(6) % 109 = 43
- Tb = g^(Sb) % p = 79^(7) % 109 = 18
Shared Secret Key:
- S = Tb^Sa % p = (g^(Sb) % p)^Sa % p = 18^6 % 109 = 82
- S' = Ta^Sb % p = (g^(Sa) % p)^Sb % p = 43^7 % 109 = 82
S = S'
(g^(Sb) % p)^Sa % p = (g^(Sa) % p)^Sb % p

#### b. With p = 11 and g = 2, suppose Alice and Bob choose private keys Sa = 5 and Sb = 12, respectively. Calculate Alice’s and Bob’s public keys, Ta and Tb. Show all work.
Ta = g^Sa mod p = 2^5 mod 11 = 10
Tb = g^Sb mod p = 2^12 mod 11 = 4

#### c. Following up on part (b), now calculate S as the shared symmetric key. Show all work.
S = Tb^Sa mod p = 4^5 mod 11 = 1
S' = Ta^Sb mod p = 10^12 mod 11 = 1

#### d. Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.
![d](p9.PNG)

### P14. The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?
OSPF does not require both sides of the connection to verify the unique key used with MAC, so it can be used instead of digital signature.

### P23. Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.
![p23](8.28.PNG)
R2 will not decrypt the duplicate datagram because Trudy's sent datagram will not have the encyption key attached and modified at the end.