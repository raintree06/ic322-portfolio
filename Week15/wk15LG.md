# Week 15 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain how two strangers are able to exchange secret keys in a public medium.
- secret keys are sent through the use of encryption
- the users have a known shared secret that is used to decrypt the private key to allow use for decryption of the message
- the message is sent through an encyption with use of both the public key and the source's private key, then the receiver decrypts the message by using the shared secret to retreive the private key

## I can walk through the TLS handshake and explain why each step is necessary.
1. requires the users (or applications) to establish a TCP Connection
2. Source user must verify that the connection with the user/application is actually the user/application it is trying to reach
3. source sends receiving user/application a master secret key used by both users/applications to generate all symmetric kets they need
![TLS handshake](tls.png)

## I can explain how TLS prevents man-in-the-middle attacks.
TLS uses sequence numbers to solve this issue.
- sequence number count allows for the user and receiver to include the sequence number in their communication, allowing for the user and receiver to confirm no one has tampered data

# Topics to Encounter During the Week:
- **Confidentiality vs. Integrity vs. Authentication**
    - confidentiality = only sender & receiver should be able to understand contents of the message (use encryption for secure communitcation)
    - integrity = content of message was not altered
    - authentication = both sender and receiver should be abe to authenticate who sent the message and if the message got to the right person
- **Symmetric Keys vs. Public Key Encryption**
    - symmetric: Sender & reciever's keys are identical and secret
        - Caesar cipher
    - public key: pair of keys is used; one is known to both sender and receiver (& is public), other is known only by either sender or  receiver (but not both)
- **Cryptanalysis Methods:**
    - **Ciphertext-Only**
        - intruder may only acccess intercepted ciphertext, with no info on the contents of the plaintext messafe
    - **Known-Plaintext**
        - when intruder knows some of the plaintext to ciphertext pairings
    - **Chosen-Plaintext**
        - intruder can choose plaintext message and obtain the corresponding ciphertext form
- **Block Ciphers**
    - message to be encrypted is processed in blocks of k bits, then the message is broken down into that k number of but blocks, and each block is encrypted individually
    - to encode, it uses one-to-one mapping to map the k-bit block of cleartext to a k-bbit block of cipher text
- **Cipher-Block Chaining (CBC)**
    - encrypting long messages or streams of data
    - need some randomness into blocks with block ciphers; some of those blocks may have the same thing and will therefore be encrypted to the same thing too! This is bad!
    - we want identical plaintext blocks to produce different ciphertext blocks
    - The basic idea is to send only one random value along with the very first message, and then have the sender and receiver use the computed coded blocks in place of the subsequent random number.
- **RSA Algorithm**
    - has become synonymous with public key cryptography
- **Digital Signatures:**
    - a cryptographic technique for achieving these goals in a digital world.
    - **Message Authentication Code (MAC)**
        - AKA message integrity problem solver
        - MAC is the code created to verify the sender is who they say they are, and is appended to the message
        - it is created from a shared secret known between the two users prior to the message being sent
    - **Public Key Certification**
        - certitifing that public keys belong to a certain entity
        - verifies that an entity is who they say their are & creates a certificate that binds the public key and globally unique identifying inforation about the owner to the public key
- **Web Authentication:**
    - **IP Spoofing**
        - sending a message and deguising it as if it were from another IP address
        - ![8.16](8.16.png)
    - **Nonce (Challenge Value)**
        - a number that a protocol will use only once in a lifetime. That is, once a protocol uses a nonce, it will never use that number again
- **Securing Email (Confidentiality and Integrity):**
    - **Encrypting a Symmetric Key with Public Keys**
    - **PGP (Pretty Good Privacy)**
        - email encryption scheme
        - software creates a public key pair for the user
        - pair can be posted on the user's website or placed in public key server, private key is protected
- **Transport Layer Security (TLS)**
    - TLS = Transport Layer Security
    - it enhaced TCP with confidentiality, data integrity, server authentication, and client authentication
    - uses sumple API with sockets
    - when application wants to employ TLS, the application includes SSL classes/libraries
- **IPSec: Layer 3 Security and VPNs:**
    - **Protocols: Authentication Header (AH), Encapsulation Security Payload (ESP)**
    - **Security Association (SA)**
    - Tunnel Mode and Transport Mode
- Wireless Security:
    - Protocols: WEP, WPA1, WPA2, WPA3
    - 4G LTE Security
- Firewalls:
    - Filter Options
    - Access Control Lists (ACLs)
    - Traditional vs. Stateful Filters
    - Application Gateways
- IDS and IPS:
    - Intrusion Detection Systems (IDS) and Intrusion Prevention Systems (IPS)
    - Deep Packet Inspections
    - Demilitarized Zone (DMZ)
    - Signature-Based vs. Anomaly-Based Detection
    - Snort
