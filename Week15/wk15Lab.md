# Week 15 Lab

**Mowery, Sydney** | ***IC322 - Networks***

<ins>The Internet Attack Blog: How to not become another Phish in the Sea</ins>

Phishing attacks can happen to anyone. Even you. How can you prevent yourself from becoming another phishing victim?
Don’t worry. We can help. We will cover everything you need to know about understanding exactly what a phishing attack is, where it comes from, how it’s used, how you can be attacked, and how to protect yourself.
Read on to learn. Or don’t, and become a phishing victim!

A phishing attack is an internet attack used to steal data [1]. It is a type of social engineering attack where an attacker pretends to be a trusted source in order to steal information from a user or organization [2]. Confused already with all the terms? We will dive into everything in the following sections!

**Social Engineering**
To begin, let’s start with defining social engineering. What the heck is that? Social engineering is a technique where criminals will manipulate others through deceit. The social part references the criminal using social norms to understand what the public would see as believable. Then the criminal disguises itself as something trustworthy in order to steal information from people [3]. 
Let’s imagine this example:
After a long Thursday of stressful back to back lab periods (aka my Thursday next semester) you are walking up to beautiful Mother B, and someone you do not recognize at all walks up to you in a working blues uniform telling you they forgot their military ID and can’t get into the homeland. Well, they are wearing blues, and they have no rank on their collar so clearly they are just a dummy plebe, so yeah why not let them in? It happens! Who doesn’t forget their CAC sometimes? You let them in, because duh who wouldn’t! Next thing you know you see a Brigade email about a crazy dangerous ‘20 grad going off the rails on cocaine and somehow got on the yard! It says: “do not interact with” and “report immediately.” You recognize the photo from the email because you let them into our Mother B! But how could you have known? They were dressed in working blues, so believable! And they had a pretty credible story, I mean who can’t relate to a forgotten CAC? And you trusted them, because the person seemed believable! Hmmm… there were some small gaps in the story though … why was the supposed plebe waiting outside the 2/C doors? And come to think of it, the uniform was pretty dirty and their hair was pretty long, at least from what you could see with the cover. Small things, though, are easily missed, especially because it's easy for people to make mistakes - maybe it was just a slimy midshipman. But those mistakes were looked passed, and now you’ve let a crazy dangerous person into your home!
You just got socially engineered! The old grad knew exactly what seemed believable and they played you like a beautiful harp in a greek melody.

**Phishing: A Social Engineering Attack**
So how is phishing a social engineering attack? Well, as discussed above, the criminal does exactly as described. Criminals disguise themselves as something believable, and send things through email, text, or phone.
For example, I have tons of emails from Dicks Sporting Goods… flash sales, clearance, discounts, new fashion items, you name it! But what about when I get an email about winning a prize from the store, and I need to “click here now” to confirm by prize? Well I get excited of course, because who doesn’t like to win? In fact, I even won a NABSD raffle prize tonight! I was so happy! (Found out it was a cribbage board though - I guess subs is destined for me.) 
Anyone in proper state of mind would clearly click wherever it tells you to, because duh! However, this is one of the easiest phishing attacks possible, because it gets the users excited about the idea of getting something for free, that they completely miss the fine details that show it is clearly from a fake email (such as this email being from “Dicks Sporting” versus the typical “DICK’s Sporting Goods). (This of course did not happen to me in any way.)
Then you click on the site, and boom it either downloads malware or it asks for you to insert your information and now they’ve stolen your private data.

**Ethical Implications**
Ethically, phishing is completely illegal. You can not simply steal people’s personal information, and there are certain ways sites must handle personal data, including allowing consent from users when inputting/uploading their data to some platform. Ethically, professionals should respond by saying it is unethical. However, it is important to consider that some companies may purposely send phishing emails to their own workers to see how their security holds up. However, it is important to get consent from all workers at an earlier time in order to “ethically” allow this kind of behavior. In that sense, it would also be important to make sure that the information being collected is not too sensitive of information either [4].

**Legal Considerations**
Federally, there is no law that makes phishing illegal, however, there are other umbrella laws that cover these kind of attacks! There are state phishing laws and other federal fraud laws that may cover the attack, but really it boils down to exactly what social engineering and technological techniques were used in the attack itself.

**Professional Responsibilities**
Professional responsibilities of any organization that wishes to be secure for themselves and for their customers would train their employees on these kinds of internet attacks. As discussed above under ethical implications, some companies may even use phishing techniques of their own simply to train and test their personnel. We will cover some basic techniques of avoid phishing in the next section.

**How do I avoid phishing attacks?**
Great question. Scratch that. Amazing question actually. Phishing attacks come from a failure to verify the source of some kind of message. As we discussed in Week 18 of our notes, we can use things such as digital signatures and message authentication codes to verify where our messages are coming from. Even public key certification is a basic way to recognize that everything is lining up correctly, and we are not being played. However, this comes in different forms in email and other phishing attacks. Basic ways to check for phishing attacks is checking the source email address. Spelling is also a major indicator for a fake message being sent from an official source (they are official - there shouldn't be any spelling errors)! Setting up 2-factor authentication can also be a life-saver for these kind of attacks [6]!
Always just watch for the small details! If something does not look right, it probably isn’t. Better safe than sorry!


_**Sources**_

1. “Top 20 most common types of cyber attacks | Fortinet,” Fortinet. https://www.fortinet.com/resources/cyberglossary/types-of-cyber-attacks
2. E. Hasson, What is phishing | Attack techniques & scam examples | Imperva. 2020. [Online]. Available: https://www.imperva.com/learn/application-security/phishing-attack-scam/
3. Carnegie Mellon University, “Social Engineering - Information Security Office - Computing Services - Carnegie Mellon University.” https://www.cmu.edu/iso/aware/dont-take-the-bait/social-engineering.html#:~:text=Social%20engineering%20is%20the%20tactic,or%20giving%20away%20sensitive%20information.
4. Admin, “Ethical Phishing: Testing your employees,” Spambrella, Sep. 26, 2023. https://www.spambrella.com/ethical-phishing-testing-your-employees/#:~:text=While%20the%20intention%20behind%20ethical,of%20a%20security%20awareness%20program.
5. M. Theoharis, “Phishing: sentencing and penalties,” www.criminaldefenselawyer.com, Nov. 14, 2023. https://www.criminaldefenselawyer.com/crime-penalties/federal/Phishing.htm#:~:text=Federal%20Laws%20on%20Phishing%3F,and%20other%20identity%20theft%20crimes.
6. “How to recognize and avoid phishing scams,” Consumer Advice, Nov. 29, 2023. https://consumer.ftc.gov/articles/how-recognize-and-avoid-phishing-scams

