# Week 12 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 6.3

### R4. Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as dprop. Will there be a collision if dprop < L/R Why or why not?
Yes. This would mean that the another node could begin trasnmitting their packet before the first node has finished.

### R5. In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?
Characteristics of Broadcast Channel:
    1. When only one node has data to send, that node has a throughput of R bps.
    2. When M nodes have data to send, each of these nodes has a throughput of R/M bps. This need not necessarily imply that each of the M nodes always has an instantaneous rate of R/M, but rather that each node should have an average transmission rate of R/M over some suitably defined interval of time.
    3. The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
    4. The protocol is simple, so that it is inexpensive to implement.
Slotted Aloha has number 1, 3, and 4.
Token Passing has number 1 and 3.

### R6. In CSMA/CD, after the fifth collision, what is the probability that a node chooses K = 4. The result K=4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?
Specifically, when transmitting a frame that has already experienced n collisions, a node chooses the value of K at random from {0,1,2,... 2^*n* - 1}
After 5 collisions, it will choose a number from 0 through 2^*5* - 1 (so 0 throgh 31). The chance of choosing K=4 is 1/32 probability. It is a delay of K*512 = 2560.

## Chapter 6 Problems

### P1. Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.
1110|1
0110|0
1001|0
0101|0
----|-
0100|1

Answer = 1

### P3. Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.
![p3 image](edc.png)
Internet: 01001001 01101110 01110100 01100101 01110010 01101110 01100101 01110100
CheckSum: 10110110 10010001 10001011 10011010 10001101 10010001 10011010 10001011

### P6. Consider the 5-bit generator, and suppose that D has the following values. What is the value of R?
* G = r + 1
#### a. 1000100101.
    R = 0010
#### b. 0101101010.
    R = 0101
#### c. 0110100011.
    R = 0001

### P11. Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.

#### a. What is the probability that node A succeeds for the first time in slot 4?
    1/4 * p

#### b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?
    p

### c. What is the probability that the first success occurs in slot 4?
    1/4 * P

### d. What is the efficiency of this four-node system?
    The four node system means there is a higher likelihood of all nodes successfully trasnmitting their data.

## P13. Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is dpoll. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?
The maxiumum throughput of the broadcast channel would then be N/R.
