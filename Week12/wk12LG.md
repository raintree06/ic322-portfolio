# Week 12 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.
- ALOHA & CSMA/CD protocols are in the Random Access Protocols category of multiple-access protocols
- random access protocols:
    - trasnmitting node always trasnmits at a full rate of the channel (*R* bps)
    - when collision occurs, each node involved in the collision repeatedly retransmits its time frame (packet) until a frame gets through without a collision
    - waits a random delay befre retransmitting the frame
        - delays are independent
- ALOHA:
    - unslotted, fully decentralized protocol
    - when frame 1st arrives, the node immediately trasnmits its entiretly into the broadcast channel
    - if collision occurs, node immediately retrasnmits frame withe probability *p*
    - else the node waits for a frame trasnmission time
        - then trasnmits the frame with probability *p* or waits for another frame with probability (1-*p*)
    - !(aloha)[aloha.png]
    - similar to someone chatting away white others are speaking
- CDMA/CD (Carrier Sense Multiple Access with Collision Detection):
    - CDMA vs CDMA/CD
    - ![cdma](csma.png) ![cdma/cd](csmacol.png)
    - adding collision detection to a multiple access protocol will help protocol performance by not transmitting a useless, damaged (by interference with a frame from another node) frame in its entirety
    - CDMA: 
        - listen before speaking: carrier sending - node listens to channel before trasnmitting
        - if another node starts trasnmitting at the same time - stop trasnmitting: collision detection
        - the end-to-end channel propagation delay of a broadcast channel—the time it takes for a signal to propagate from one of the nodes to another—will play a crucial role in determining its performance
    - an adapter in a node does this:
        - obtains datagram from network later, prepares a link-layer frame and buts the frame apadter buffer
    - if the adaper sense that the channel is idle, it starts to trasnmit the frame
    - if adapter senses channel is busy, it waits until no singal energy to send
    - adapter monitors presence of signal energy coming from other adapters using the broadcast channel
    - if adapter detects signal energy from other adapters while trasnmitting, it aborts the transmission and tries again from the beginning (sensing when channel is empty)
    - uses binary exponential backoff algorithm to decinde how much time to pass between trying again to tramit if a collision was detected


## I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.
parity bits:
- sender simply includes one additional bit to the information to be sent
- even parity schemes: if data has d bits, we want the total number of bits in d + the extra bit to have an even number of 1s
    - ![even parity example](parity.png)
- odd parity scheme: same thing but we want toal number of 1s to be odd
- receiver end:
    - count the number of 1s in the (d+1) bits
        - if odd number found in even parity scheme, receiver knows at least one bit of error has occured (knows some *odd* number of bit errors occured)

2-D even parity:
    - ![2d even parity](2par.png)
    - *i* + *j* + 1 parity bits comprise the link-layer frame's error-detection bits
    - receiver can detect and use the col and row indices of the column and row parity errors to actually identify the bit that was corrupted and correct that error
        - this is known as **forward error correction (FEC)**

internet checksum:
- checksum:
    - *d* bits of data treated as a sequence of *k*-bit integers
    - sum *k*-bit integers & use the resulting sum as the error-detection bits
- bytes of data are treated as 16-bit integers and summed
1s complement of this sum then forms the internet checksum by taking the 1s complement of the sum of the received data (including the checksum) and checking whether the sesult is all 0 bits
    - if bits are 1 then error occured
- requires little packet overhea
- weak protection against errors as compared with cyclic redundancy check
- error detection at the link later is implemented in the hardware

Cyclic Redundancy Check (CRC)
- error detection at the link later is implemented in the hardware which can rapidly perform complex CRC
- based on cyclic redundancy check (CRC) codes, which are also known as polynomial codes
- you have *d* bit piece of data to send
    - sender & receiver must agress on an *r* + 1 bit pattern, known as the gernator (called *G*)
    - most significant (leftmost) bit of G must be 1
    - sender chooses *r* additonal bits and appends then to *d* such that the resulting *d* + *r* pattern is exactly divisible by *G*
        - = ((*d* + *r*) / (*r* + 1))
        - if remainder is nonzero, error occured
- ![crc](crc.png)
- all calculations done in mod-2 arithmetic wihout carries in addition or borrows in subtraction (addition & subtraction are identical & equal to XOR)
    - Ex: 1011 XOR 0101 = 1110
    - Ex: 1001 XOR 1101 = 0100
    - Ex: 1011 - 0101 = 1110
    - Ex: 1001 - 1101 = 0100
- crc division: all addition & subtraction have no borrows or carries
    - ![crc division](crcdiv.png)
- can detect burst errors of fewer that *r* + 1 bits
- each of the CRC standards can detect any odd number of bit errors


## I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ.
- many acronyms for the protocols (ex: 10BASE-T, 10BASE-2, 100BASE-T, 1000BASE-LX, 10GBASE-T and 40GBASE-T)
    - 1st paer = speed of the standard (MB per sec)
    - “BASE” refers to baseband Ethernet, meaning that the physical media only carries Ethernet traffic; almost all of the 802.3 standards are for baseband Ethernet
    - final part = physical medium
- 10BASE-2 & 10BASE-5:
    - 10 Mbps
    - 2 cable, limited to 500 meters long
- repeater allows longer runs: physical later device that receives a signal on the input side, and regenerates the signal on the output side
- most ethernet today have nodes connected to a switch via point-to-point segments made of twisted-pair copper wires or fiber-optic cables
- standardized at 100 mbps
    - original Ethernet MAC protocol and frame format were preserved, but higher-speed physical layers were defined for copper wire (100BASE-T) and fiber (100BASE-FX, 100BASE-SX, 100BASE-BX)
    - ![100 mbps](100mb.png)
- gigabit ethernet:
    - uses standard ethernet frame format
    - point-to-point links & broadcast channels = buffered distributors
    - uses CSMA/CD for shared broadcast channels
    - allows for full-duplex operation at 40 Gbps in both directions for point-to-point channels

# Week 12 Notes
* **Nodes vs Links**
    - node = any device that runs a link layer
    - links = communication channels
* **Link-Layer Frame**
    - purpose is to move a datagram from one node to an adjacent node over a single communication link
* **Services Provided:**
    - **Framing**
        - encapsulate network-layer datagram within a link-layer frame before trasnmission over the link
    - **Link Access**
        - medium access control (MAC) protocol specifies the rules by which a frame is transmitted onto the link
        - MAC serves to coordinate the frame tranmissions of the many nodes
    - **Reliable Delivery**
        - guarentees to move each network-later datagram across the link without error
        - often used for links that are prone to high error rates
        - many wire link-layer protocols do not provide this
    - **Error-Detection and Correction**
        - The link-layer hardware in a receiving node can incorrectly decide that a bit in a frame is zero when it was transmitted as a one, and vice versa
        - has transmittion node include error-detection bits in the fram and having the receiving node perform an error check
- **NIC (Network Interface Card)/Network Adapter**
    - for the most part, link layer is implemented on a chip called the network adapter, aka a network interface controller (NIC)
    - implements many link later services
    - much of the functionality is implemented in hardware
    - ![network adapter](adapter.png)
- **Bit-Level Error Detection and Correction:**
    - detecting & correcting the corruption of bits in a link later frame sent from one node to another physically connected neighboring node
    - ![edc](edc.png)
        - sending node D is send with error-detection and correction bits EDC
        - data to be protected = datagram & link-level addressing info, sequence numbers, and other info
    - does not always work
    - **Parity Bits**
    - **2D Parity**
    - **Internet Checksum**
    - **CRC (Cyclic Redundancy Check)**
- **Point-to-Point vs Broadcast Links**
    - point-to-point link consists of single sender at one end and single receiver at other end
        - many link-layer protocols designed this way
        - point-to-point-protocol (PPP & high-level data link control (HDLC) are 2 of the protocols)
    - broadcast link can have multiple sending and receiveing nodes all connected to the same single shared broadcast channel
        - broadcast refers to when any one node transmits a frame, the channel broadcasts the frame and each of the other nodes receives a copy
        - ethernet & wireless LANs are examples of broadcast link-layer technologies
        - problem: how to coordinate the access of multiple sending and recieving nodes to a shared broadcast channel (aka the multiple access problem)
        - broadcast channel:
            1. When only one node has data to send, that node has a throughput of R bps.
            2. When M nodes have data to send, each of these nodes has a throughput of R/M bps. This need not necessarily imply that each of the M nodes always has an instantaneous rate of R/M, but rather that each node should have an average transmission rate of R/M over some suitably defined interval of time.
            3. The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
            4. The protocol is simple, so that it is inexpensive to implement.
- **The Multiple-Access Problem**
    - problem: how to coordinate the access of multiple sending and recieving nodes to a shared broadcast channel (aka the multiple access problem)
- **Multiple Access Protocols**
    - protocols for how nodes regulate their trasnmission protocols
    - ![protocols](protocol.png)
    - we need this due to frame collisions which cause a loss of frame packets
    - there are 3 categories:
        - channel partitioning protocols
        - random access protocols
        - taking-turns protocols
    - these should be able to:
        - When only one node has data to send, that node has a throughput of R bps
        - When M nodes have data to send, each of these nodes has a throughput of R/M bps
        - decentralized
        - simple so that it is inexpensive
- **Frame Collisions**
    - more than 2 nodes can trasmit frames at the same time
    - this means frames receive miltiple nodes at the same time, so the frames collide at all of the receivers
    - all frames involved are lost
    - must coordinate the trasnmissions of the active nodes
        - responsibility of the multiple access protocol
- **Channel Partitioning:**
    - time-division multiplexing (TDM) and frequency-division multiplexing (FDM) are two techniques that can be used to partition a broadcast channel’s bandwidth among all nodes sharing that channel
    - ![tdm vs fdm](tdmfdm.png)
    - **TDM (Time Division Multiplexing)**
        - divides time into time frames and further divides each time frame into N time slots
        - each time slot is then assigned to one of the N noes
        - whenever a node has a packet to send, it trasnmits the packet's bits during its assigned time slot in the revolving TDM frame
        - typically slots are chosen so that a single packet can be 
        transmitted during a slot time
        - pros:
            - eliminates collisions and is perfectly fair
            - each node gets dedicated trasnmission rate of *R*/*N* bps during each time frame
        - cons:
            - node is limited to avg rate of *R*/*N* bps evem when it is the only node with packets to send
            - node must always wait for its turn in the trasnmission sequence (even when it is the only node sending)
        - poor choice for multiple access protocol when only one node sending and all nodes listening
    - **FDM (Frequency Division Multiplexing)**
        - divides the *R* bps channel into different frequencies (each with bndwidth *R*/*N*) and assigns each frequency to one of the *N* nodes
        - creates *N* smaller channels of *R*/*N* nps out of the single, larger *R* bps channel
        - same pros & cons as TDM
            - avoids collisions and divides the bandwidth fairly among the *N* nodes
            - node limited to bandwidth *R*/*N* even when it is the only one sending
    - **CDMA (Code Division Multiple Access)**
        - assigns a different code to each node
        - each node uses code to encode the data bits it sends
        - different nodes can trasmit simultaneously and yet have their respective receivers corretly receive a sender's encoded data bits
        - used in military systems (anti-jamming properties)
        - tightly tied to wireless channels
- **Random Access Protocols:**
    - trasnmitting node always trasnmits at a full rate of the channel (*R* bps)
    - when collision occurs, each node involved in the collision repeatedly retransmits its time frame (packet) until a frame gets through without a collision
    - waits a random delay befre retransmitting the frame
        - delays are independent
    - **ALOHA**
    - **Slotted ALOHA**
        - Assume the following:
            - all frames have *L* bits
            - time divided into slots of size *L*/*R*
            - nodes trasnmit frames only at the beginning of slots
            - nodes are synchronized so each node knows when slot begins
            - if 2 more more frames collide in a slot, then all nodes detect collision before slot ends
                - if this happens, the node retrasnmits its frame in each subsequent slot with probability *p* (number btwn 0 & 1) until frame is trasnmitted w/out collision
        - allows a node to trasnmit continuously at a full rate *R* when that node is the only active one
        - decentralized (all nodes independently decide when to retrasnmit)
        - requires slots be synchronized with the nodes
        - extremely simple
        - works well when only 1 active node
        - certain fraction of the slots will have collisions and therefore be wasted when there are multiple sedning nodes
        - another fraction of slots will be empty bec all active nodes refrain from trasmitting for some time (with probability retrasnmit time)
        - successful slot = slot in which one node trasnmits
        - ![slotted aloha](slottedaloha.png)
            - Nodes 1, 2, and 3 collide in the first slot. Node 2 finally succeeds in the fourth slot, node 1 in the eighth slot, and node 3 in the ninth slot
    - **CSMA (Carrier Sense Multiple Access)**
        - in aloha protocols: nodes decsioon to trasnmit is made independely of the activity of the other nodes atached to the broadcast channel
            - node either pays attention to when it begins to trasnmit ot stops trasmitting if another node begins to interfere with the trasnmission
            - similar to someone chatting away even if others are talking
        - listen before speaking: carrier sending - node listens to channel before trasnmitting
        - if another node starts trasnmitting at the same time - stop trasnmitting: collision detection
        - !(csma)[csma.png]
        - the end-to-end channel propagation delay of a broadcast channel—the time it takes for a signal to propagate from one of the nodes to another—will play a crucial role in determining its performance
    - **CSMA/CD (Carrier Sense Multiple Access/Collision Detection)**
- **Taking-Turns Protocols:**
    - what we really want is:
        1. when only one node is active, the active node has a throughput of R bps
        2. when M nodes are active, then each active node has a throughput of nearly R/M bps
    - ALOHA & CSMA have the first but hot the second
        - this is why researchers have made the taking-turns protocol
    - 2 most important: polling protocol & token passing
    - **Polling Protocols**
        - requires one of the nodes to be designated as the master node
        - master node polls each of the nodes in a round robin fashion
        - tells each node when it can process each's frames
        - pros:
            - eliminates collisions and empty slots
            - acheives a much higher efficency
        - cons:
            - polling delay: amount of time required to notify a node that it can transmit
            - if master node fials, entire channel is inoperative
    - **Token Passing**
        - no master node
        - small, special purpose frame known as a token is exchanged among the nodes in some fixed order
        - when node receives token, it holds token only if it has some frames to trasnmit, otherwise it passes it on to the next node
        - like a talking stick; whoever has stick can talk but then must pass it on
        - if node has frames to send when it gets the tokehn, it sends up to a maximum number of frames and then forwards the token to the next node
        - decentralized, highly efficent
        - cons:
            - failure of none node can crash entire channel
            - node neglects to release token, there must be a recovery procedure to invoke to get the token moving again
- **Everything Together: DOCSIS (Data Over Cable Service Interface Specification)**
    - cable access network will make use of all three categories aspects (channel partitioning protocols, random access protocols, taking-turns protocols)
    - connects several thousand residental cable modems to a cable model termination system (CMTS) at the cable network headend
    - Data-Over-Cable Service Interface Specifications (DOCSIS) specifies the cable data network architecture and its protocols
    - uses FDM to divide downstream and upstream netwrk segments into frequency channels
    - Each upstream and downstream channel is a broadcast channel
    - Each upstream channel divided into intervals of time (TDM-like), each containing a sequence of mini-slots during which cable modems can transmit to the CMTS
    - sends a control message known as a MAP message on a downstream channel to specify which cable modem (with data to send) can transmit during which mini-slot for the interval of time specified in the control message
    - cable modems send mini-slot-request frames to the CMTS during a special set of interval mini-slots that are dedicated for this purpose
    - able network is an example of multiple access protocols in action: FDM, TDM, random access, and centrally allocated time slots all within one network
    - ![docsis](docsis.png)
- **Ethernet:**
    - most prevalent iwred LAN technology
    - widely deployed high-speed LAN
    - why ethernet?
        - admins familiar with it
        - token ring, FDDI, and ATM were more complex and expensive than ethernet, which further discouraged network administrators from switching from LANs
        - higher data rate of new technology with LANs
    - Ethernet with a bus topology is a broadcast LAN; all transmitted frames travel to and are processed by all adapters connected to the bus
    - hosts and routers are directly connected to a hub with twisted pair copper wire
    - hub = physical-layer device that acts on individual bits rather than frames
    - when a bit (0 or 1) arrives from one interface, the hub recreates the but, boosts its energy and strength, and transmits the but onto all other interfaces
    - Ethernet with a hub-based star topology is also a broadcast LAN—whenever a hub receives a bit from one of its interfaces, it sends a copy out on all of its other interfaces
        - if a hub receives frames from two different interfaces at the same time, a collision occurs and the nodes that created the frames must retransmit
    - hub replaced with a switch
        - “collision-less” and a bona-fide store-and-forward packet switch; - unlike routers, which operate up through layer 3, a switch operates only up through layer 2
    - **Frame Structure**
        - ![ethernet frame](frame.png)
        - sending adapter encapsulates the IP datagram within an Ethernet frame and passes the frame to the physical layer
        - receiving adapter receives the frame from the physical layer, extracts the IP datagram, and passes the IP datagram to the network layer
        - fields:
            - data field: carries IP datagram
            - destination address: MAC address of destination adapter
            - source address: MAC address of the adapter that trasnmits the frame onto the LAN
            - type field: permits ethernet to multiplex network-layer protocols 
            - Cyclic redundancy check (CRC): allow the receiving adapter, adapter B, to detect bit errors in the frame
            - preamble: serve to “wake up” the receiving adapters and to synchronize their clocks to that of the sender’s clock
        - Ethernet technologies provide connectionless service to the network layer
        - Ethernet technologies provide an unreliable service to the network layer
    - **Standards: 10BASE-T, 100BASE-T, etc.**
- **Repeaters**
