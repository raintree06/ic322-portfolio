# Week 12: Link Layer

**Mowery, Sydney** | ***IC322 - Networks***

* [Week 12 Vegetables](Veggies.md)
* [Week 12 Learning Goals](wk12LG.md)
* [Week 12 Lab](https://gitlab.com/jldowns-usna/protocol-pioneer)
* [Week 12 Peer Review](https://gitlab.com/dkreidler1/ic322-portfolio/-/issues/24)

