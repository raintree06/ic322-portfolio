# Week 10 Veggies

**Mowery, Sydney** | ***IC322 - Networks***

## Section 5.4

### R11. How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?
* Next-hop is the ip address of the router interface that begins the AS-PATH
* BGP route is written as a list with three components: NEXT-HOP; AS-PATH; destination prefix

### R13. True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.
* true
* When a router advertises a prefix across a BGP connection, it includes with the prefix several BGP attributes. In BGP jargon, a prefix along with its attributes is called a route

## Section 5.6

### R19. Names four different types of ICMP messages
![Different Types of ICMP Messages](<icmp.png>)

### R20. What two types of ICMP messages are received at the sending host executing the Traceroute program?
type 11, code 0 & type 3, code 3

## Problems

### P14. Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.
![p14 prob](<p15.PNG.PNG>)

#### a. Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?
* eBGP

#### b. Router 3a learns about x from which routing protocol?
* iBGP

#### c. Router 1c learns about x from which routing protocol?
* eBGP

#### d. Router 1d learns about x from which routing protocol?  
* iBGP

### P15. Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.

#### a. Will I be equal to I1 or I2 for this entry? Explain why in one sentence.
* I1
* shortest next-hop path

#### b. Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to  or ? Explain why in one sentence.
* I2
* closest nexthop router

#### c. Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to  or ? Explain why in one sentence.
* I1
* shortest as-path

### P19. In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?
![Figure 5.13](<fig5.PNG.png>)
* A advertises:
    * to B: A-W & A-V
    * to C: A-V 
* C receives:
    * A-V

### ​P20. Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?​
* yes

