# Week 10 Learning Goals

**Mowery, Sydney** | ***IC322 - Networks***

## I can explain what autonomous systems are and their significance to the Internet.
* Autonomous Systems are a way to organize routers
* Each consists of a group of routers under the same administrative control
    * routers in an ISP and links that interconnect them constitute a single AS
* Some ISPs partition their network into multiple ASs
* Identified by its globally unique autonomous system number (ASN)
    * Like IP addresses, are assigned by ICANN regional registries
* Properties:
    * Routers within the same AS all run the same routing algorithm
    * Routing algorithm running is AS is called the intra-autonomous system routing protocol

## I can describe how the BGP protocol works as well as why and where it is used.
* inter-AS routing protocol
* BGP glues thousands of ISPs in the itnernet together
* BGP provides each router:
    * Ability to obtain prefix reachability info from neighbors
        * Can advertise its existence to the rest of the internet
    * Ability to determine the best routes to the prefixes	
        * Runs BGP route-selection procedure

## I can explain what the ICMP protocol is used for, with concrete examples.
* ICMP = Internet Control Message Protocol
* Used by hosts and routers to communicate network-layer info to each other
* Typical use is error reporting
* Used by
    * Traceroute
    * Ping
* ICMP Types & Description
    ![ICMP Types](<icmp.png>)
